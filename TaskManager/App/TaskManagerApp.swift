//
//  TaskManagerApp.swift
//  TaskManager
//
//  Created by Manuel Meyer on 14.06.22.
//

import SwiftUI
import TaskManagerUI
import TaskManagerModels
import TaskManagerApp

@main
final class TaskManagerApp: App {
    private let store: Store = createDiskStore()
    private lazy var rootHandler: (Message) -> ()
    = createAppDomain(
        store      : store,
        receivers  : [],
        rootHandler: { self.rootHandler($0) }
    )
    var body: some Scene { WindowGroup { ContentView(store:store, rootHandler:rootHandler)} }
}
