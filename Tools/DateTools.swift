import Foundation

public extension Date {
    static var        today:Date { Date().midnight                               }
    static var    yesterday:Date { Date.today.dayBefore                          }
    static var     tomorrow:Date { Date.today.dayAfter                           }
    static var  aDayFromNow:Date { cal.date(byAdding:.day, value: 1, to:Date())! }
    static var aWeekFromNow:Date { cal.date(byAdding:.day, value: 7, to:Date())! }
    static var  aWeekBefore:Date { cal.date(byAdding:.day, value:-7, to:Date())! }
    
    var   dayBefore:Date { cal.date(byAdding:.day, value:-1,              to:self)! }
    var    dayAfter:Date { cal.date(byAdding:.day, value: 1,              to:self)! }
    var  aWeekLater:Date { cal.date(byAdding:.day, value: 7,              to:self)! }
    var    midnight:Date { cal.date(bySettingHour: 0, minute:0, second:0, of:self)! }
    var        noon:Date { cal.date(bySettingHour:12, minute:0, second:0, of:self)! }

    private static var cal:Calendar { .current }
    private        var cal:Calendar { .current }
}
