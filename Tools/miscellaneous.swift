//
//  miscellaneous.swift
//  BertTools
//
//  Created by vikingosegundo on 17/02/2022.
//

public func -<T: RangeReplaceableCollection>(lhs:T, rhs:T) -> T where T.Iterator.Element: Equatable {
    var lhs = lhs
    for element in rhs {
        if let index = lhs.firstIndex(of:element) { lhs.remove(at:index) }
    }
    return lhs
}
