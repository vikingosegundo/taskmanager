//
//  SubTaskSpec.swift
//  TaskManagerModelsTests
//
//  Created by Manuel Meyer on 26.06.22.
//

import Quick
import Nimble
@testable import TaskManagerModels

fileprivate let manu    = Collaborator(name:"Manu" )
fileprivate let project = Project(  owner:manu   ,title:"project"  )
fileprivate let task    = Task   (project:project.id,title:"task 0"   )
fileprivate let subtask = SubTask(   task:task   ,title:"subtask 0")
fileprivate let address = Location.Address(street:"street", city:"city")
fileprivate let coord   = Location.Coordinate(latitude:53.2190827, longitude:6.5773836)

final class SubTaskSpec: QuickSpec {
    override func spec() {
        describe("SubTask") {
            context("creation") {
                it("is created with id"               ) { expect(subtask.id       ).toNot(beNil  ()            ) }
                it("is created with task id"          ) { expect(subtask.task     ).to   (equal  (task.id)     ) }
                it("is created with title"            ) { expect(subtask.title    ).to   (equal  ("subtask 0") ) }
                it("is created with empty text"       ) { expect(subtask.text     ).to   (beEmpty()            ) }
                it("is created incomplete"            ) { expect(subtask.completed).to   (beFalse()            ) }
                it("is created without given location") { expect(subtask.location ).to   (equal  (.unknown)    ) }
                it("is created unfinished"            ) { expect(subtask.state    ).to   (equal  (.unfinished) ) }
            }
            context("alteration") {
                context("change title") {
                    let n = alter(subtask, by:.changing(.title(to:"SubTask 0")))
                    it("title is changed"     ) { expect(n.title    ).to(equal("SubTask 0")      ) }
                    it("id is unchanged"      ) { expect(n.id       ).to(equal(subtask.id)       ) }
                    it("task hasnt change"    ) { expect(n.task     ).to(equal(subtask.task)     ) }
                    it("is still incomplete"  ) { expect(n.completed).to(equal(subtask.completed)) }
                    it("text isnt changed"    ) { expect(n.text     ).to(equal(subtask.text)     ) }
                    it("location isnt changed") { expect(n.location ).to(equal(subtask.location) ) }
                    it("state isnt change"    ) { expect(n.state    ).to(equal(subtask.state)    ) }
                }
                context("change text") {
                    let n = alter(subtask, by:.changing(.text(to:"SubTask 0")))
                    it("text is changed"      ) { expect(n.text     ).to(equal("SubTask 0")      ) }
                    it("id is unchanged"      ) { expect(n.id       ).to(equal(subtask.id)       ) }
                    it("task hasnt change"    ) { expect(n.task     ).to(equal(subtask.task)     ) }
                    it("is still incomplete"  ) { expect(n.completed).to(equal(subtask.completed)) }
                    it("title isnt changed"   ) { expect(n.title    ).to(equal(subtask.title)    ) }
                    it("location isnt changed") { expect(n.location ).to(equal(subtask.location) ) }
                    it("state isnt change"    ) { expect(n.state    ).to(equal(subtask.state)    ) }
                }
                context("change state") {
                    let n = alter(subtask, by:.changing(.completed(to:true)))
                    it("is completed"         ) { expect(n.completed).to(beTrue()                ) }
                    it("id is unchanged"      ) { expect(n.id       ).to(equal(subtask.id)       ) }
                    it("title unchanged"      ) { expect(n.title    ).to(equal(subtask.title)    ) }
                    it("task hasnt change"    ) { expect(n.task     ).to(equal(subtask.task)     ) }
                    it("text isnt changed"    ) { expect(n.text     ).to(equal(subtask.text)     ) }
                    it("location isnt changed") { expect(n.location ).to(equal(subtask.location) ) }
                    it("state isnt change"    ) { expect(n.state    ).to(equal(subtask.state)    ) }
                }
                context("change task") {
                    let t1 = Task(project:project.id, title:"new task")
                    let n = subtask.alter(by:.changing(.task(to:t1.id)))
                    it("task id changed"      ) { expect(n.task     ).to(equal  (t1.id)            ) }
                    it("is incomplete"        ) { expect(n.completed).to(beFalse()                 ) }
                    it("title unchanged"      ) { expect(n.title    ).to(equal  (subtask.title)    ) }
                    it("id is unchanged"      ) { expect(n.id       ).to(equal  (subtask.id)       ) }
                    it("text isnt changed"    ) { expect(n.text     ).to(equal  (subtask.text)     ) }
                    it("location isnt changed") { expect(n.location ).to(equal  (subtask.location) ) }
                    it("state isnt change"    ) { expect(n.state    ).to(equal  (subtask.state)    ) }
                }
                context("change location") {
                    context("from unknow to coord") {
                        let n = subtask.alter(by:.changing(.location(to:.coordinate(coord))))
                        it("location changed to coord") { expect(n.location ).to(equal(.coordinate(coord)) ) }
                        it("task id hasnt change"     ) { expect(n.task     ).to(equal(subtask.task)       ) }
                        it("title unchanged"          ) { expect(n.title    ).to(equal(subtask.title)      ) }
                        it("id is unchanged"          ) { expect(n.id       ).to(equal(subtask.id)         ) }
                        it("text isnt changed"        ) { expect(n.text     ).to(equal(subtask.text)       ) }
                        it("is incomplete"            ) { expect(n.completed).to(beFalse()                 ) }
                        it("state isnt change"        ) { expect(n.state    ).to(equal(subtask.state)      ) }
                    }
                    context("from unknow to address") {
                        let n = subtask.alter(by:.changing(.location(to:.address(address))))
                        it("location changed to coord") { expect(n.location ).to(equal(.address(address)) ) }
                        it("task id hasnt change"     ) { expect(n.task     ).to(equal(subtask.task)      ) }
                        it("title unchanged"          ) { expect(n.title    ).to(equal(subtask.title)     ) }
                        it("id is unchanged"          ) { expect(n.id       ).to(equal(subtask.id)        ) }
                        it("text isnt changed"        ) { expect(n.text     ).to(equal(subtask.text)      ) }
                        it("is incomplete"            ) { expect(n.completed).to(beFalse()                ) }
                        it("state isnt change"        ) { expect(n.state    ).to(equal(subtask.state)     ) }
                    }
                    context("from address to unknow") {
                        let n = subtask
                            .alter(by:.changing(.location(to:.address(address))))
                            .alter(by:.changing(.location(to:.unknown)))
                        it("location changed to unknown") { expect(n.location ).to(equal(.unknown)      ) }
                        it("task id hasnt change"       ) { expect(n.task     ).to(equal(subtask.task)  ) }
                        it("title unchanged"            ) { expect(n.title    ).to(equal(subtask.title) ) }
                        it("id is unchanged"            ) { expect(n.id       ).to(equal(subtask.id)    ) }
                        it("text isnt changed"          ) { expect(n.text     ).to(equal(subtask.text)  ) }
                        it("is incomplete"              ) { expect(n.completed).to(beFalse()            ) }
                        it("state isnt change"          ) { expect(n.state    ).to(equal(subtask.state) ) }
                    }
                    context("from coord to unknow") {
                        let n = subtask
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                            .alter(by:.changing(.location(to:.unknown)))
                        it("location changed to unknown") { expect(n.location ).to(equal(.unknown)      ) }
                        it("task id hasnt change"       ) { expect(n.task     ).to(equal(subtask.task)  ) }
                        it("title unchanged"            ) { expect(n.title    ).to(equal(subtask.title) ) }
                        it("id is unchanged"            ) { expect(n.id       ).to(equal(subtask.id)    ) }
                        it("text isnt changed"          ) { expect(n.text     ).to(equal(subtask.text)  ) }
                        it("is incomplete"              ) { expect(n.completed).to(beFalse()            ) }
                        it("state isnt change"          ) { expect(n.state    ).to(equal(subtask.state) ) }
                    }
                    context("from address to coord") {
                        let n = subtask
                            .alter(by:.changing(.location(to:.address(address))))
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                        it("location changed to unknown") { expect(n.location ).to(equal(.coordinate(coord)) ) }
                        it("task id hasnt change"       ) { expect(n.task     ).to(equal(subtask.task)       ) }
                        it("title unchanged"            ) { expect(n.title    ).to(equal(subtask.title)      ) }
                        it("id is unchanged"            ) { expect(n.id       ).to(equal(subtask.id)         ) }
                        it("text isnt changed"          ) { expect(n.text     ).to(equal(subtask.text)       ) }
                        it("is incomplete"              ) { expect(n.completed).to(beFalse()                 ) }
                        it("state isnt change"          ) { expect(n.state    ).to(equal(subtask.state)      ) }
                    }
                    context("from coord to address") {
                        let n = subtask
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                            .alter(by:.changing(.location(to:.address(address))))
                        it("location changed to address") { expect(n.location ).to(equal(.address(address)) ) }
                        it("task id hasnt change"       ) { expect(n.task     ).to(equal(subtask.task)      ) }
                        it("title unchanged"            ) { expect(n.title    ).to(equal(subtask.title)     ) }
                        it("id is unchanged"            ) { expect(n.id       ).to(equal(subtask.id)        ) }
                        it("text isnt changed"          ) { expect(n.text     ).to(equal(subtask.text)      ) }
                        it("is incomplete"              ) { expect(n.completed).to(beFalse()                ) }
                        it("state isnt change"          ) { expect(n.state    ).to(equal(subtask.state)     ) }
                    }
                }
                context("change state") {
                    context("from unfinished to finished") {
                        let n = subtask.alter(by:.changing(.state(to:.finisehd)))
                        it("state is finished"    ) { expect(n.state    ).to(equal(.finisehd)        ) }
                        it("task id hasnt change" ) { expect(n.task     ).to(equal(subtask.task)     ) }
                        it("title unchanged"      ) { expect(n.title    ).to(equal(subtask.title)    ) }
                        it("id is unchanged"      ) { expect(n.id       ).to(equal(subtask.id)       ) }
                        it("text isnt changed"    ) { expect(n.text     ).to(equal(subtask.text)     ) }
                        it("is incomplete"        ) { expect(n.completed).to(beFalse()               ) }
                        it("location is unchanged") { expect(n.location ).to(equal(subtask.location) ) }
                    }
                }
                context("equal alter interfaces") { // subtask.alter(by:...) == alter(subtask,by:...)
                    context("change title") {
                        let n0 = alter(subtask,by:.changing(.title(to:"SubTask 0 title")))
                        let n1 = subtask.alter(by:.changing(.title(to:"SubTask 0 title")))
                        it("both results are equal") { expect(n0).to(equal(n1)) }
                    }
                    context("change text") {
                        let n0 = alter(subtask,by:.changing(.text(to:"SubTask 0 text")))
                        let n1 = subtask.alter(by:.changing(.text(to:"SubTask 0 text")))
                        it("both results are equal") { expect(n0).to(equal(n1))}
                    }
                    context("change state") {
                        let t = Task(project:project.id,title:"new task")
                        let n0 = alter(subtask,by:.changing(.task(to:t.id)))
                        let n1 = subtask.alter(by:.changing(.task(to:t.id)))
                        it("both results are equal") { expect(n0).to(equal(n1)) }
                    }
                    context("change task") {
                        let n0 = alter(subtask,by:.changing(.completed(to:true)))
                        let n1 = subtask.alter(by:.changing(.completed(to:true)))
                        it("both results are equal") { expect(n0).to(equal(n1)) }
                    }
                }
            }
            context("coding") {
                let enc = try! JSONEncoder().encode(subtask)
                let dec = try! JSONDecoder().decode(SubTask.self, from: enc)
                context("encoding") {
                    it("subtask to data") { expect(enc).to(beAKindOf(Data.self)) }
                }
                context("decoding") {
                    it("subtask from data"           ) { expect(dec      ).to(beAKindOf(SubTask.self)) }
                    it("subtask same id as before"   ) { expect(dec.id   ).to(equal(subtask.id)      ) }
                    it("subtask same title as before") { expect(dec.title).to(equal(subtask.title)   ) }
                    it("text isnt changed"           ) { expect(dec.text ).to(equal(subtask.text)    ) }
                    it("subtask same task as before" ) { expect(dec.task ).to(equal(subtask.task)    ) }
                    it("state isnt change"           ) { expect(dec.state).to(equal(subtask.state)   ) }
                }
            }
            context("print") {
                it("description") { expect("\(subtask)").to(equal("SubTask «\(subtask.id)» on Task «\(task.id)»: \(subtask.title), completed: \(subtask.completed)")) }
            }
            context("sets") {
                let setOfSubTasks = Set([subtask])
                let n = setOfSubTasks.first(where:{ $0.id == subtask.id })!
                it("creates a set of one subtasks") { expect(setOfSubTasks).to(haveCount(1)     ) }
                it("retrives subtask by id"       ) { expect(n.id         ).to(equal(subtask.id)) }
            }
        }
    }
}
