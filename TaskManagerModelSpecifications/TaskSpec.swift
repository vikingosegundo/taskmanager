//
//  TaskSpec.swift
//  TaskManagerModelsTests
//
//  Created by Manuel Meyer on 26.06.22.
//
import Quick
import Nimble
@testable import TaskManagerModels

fileprivate let manu    = Collaborator(name:"Manu" )
fileprivate let project = Project(  owner:manu   ,title:"project"  )
fileprivate let task    = Task   (project:project.id,title:"task 0"   )
fileprivate let subtask = SubTask(   task:task   ,title:"subtask 0")
fileprivate let address = Location.Address(street:"street", city:"city")
fileprivate let coord   = Location.Coordinate(latitude:53.2190827, longitude:6.5773836)

final class TaskSpec: QuickSpec {
    override func spec() {
        describe("Task") {
            context("creation") {
                it("is created with an id"           ) { expect(task.id      ).toNot(beNil()                       ) }
                it("is created with title"           ) { expect(task.title   ).to   (equal("task 0")               ) }
                it("is created with project id"      ) { expect(task.project ).to   (equal(project.id)             ) }
                it("is created with empty text"      ) { expect(task.text    ).to   (beEmpty()                     ) }
                it("is created with unknown location") { expect(task.location).to   (equal(.unknown)               ) }
                it("is created unfinished"           ) { expect(task.state   ).to   (equal(.unfinished(.unstarted))) }
            }
            context("alteration") {
                context("change title") {
                    let n = alter(task,by:.changing(.title(to:"Task 0")))
                    it("title is changed"     ) { expect(n.title   ).to(equal("Task 0")     ) }
                    it("task id hasnt change" ) { expect(n.id      ).to(equal(task.id)      ) }
                    it("text isnt changed"    ) { expect(n.text    ).to(equal(task.text)    ) }
                    it("location isnt changed") { expect(n.location).to(equal(task.location)) }
                    it("state isnt changed"   ) { expect(n.state   ).to(equal(task.state)   ) }
                }
                context("change text") {
                    let n = alter(task,by:.changing(.text(to:"Task 0 Text")))
                    it("text changed"         ) { expect(n.text    ).to(equal("Task 0 Text")) }
                    it("title isnt changed"   ) { expect(n.title   ).to(equal(task.title)   ) }
                    it("task id hasnt change" ) { expect(n.id      ).to(equal(task.id)      ) }
                    it("location isnt changed") { expect(n.location).to(equal(task.location)) }
                    it("state isnt changed"   ) { expect(n.state   ).to(equal(task.state)   ) }
                }
                context("change project") {
                    let p = Project(owner:manu,title:"Project 1")
                    let n = alter(task,by:.changing(.project(to:p.id)))
                    it("changed to new"       ) { expect(n.project ).to   (equal(p.id)         ) }
                    it("changed from old"     ) { expect(n.project ).toNot(equal(project.id)   ) }
                    it("title is unchanged"   ) { expect(n.title   ).to   (equal(task.title)   ) }
                    it("task id hasnt change" ) { expect(n.id      ).to   (equal(task.id)      ) }
                    it("text isnt changed"    ) { expect(n.text    ).to   (equal(task.text)    ) }
                    it("location isnt changed") { expect(n.location).to   (equal(task.location)) }
                    it("state isnt changed"   ) { expect(n.state   ).to   (equal(task.state)   ) }
                }
                context("equal alter interfaces") { // task.alter(by:...) == alter(task,by:...)
                    context("change title") {
                        let t0 = alter(task,by:.changing(.title(to:"Task 0")))
                        let t1 = task.alter(by:.changing(.title(to:"Task 0")))
                        it("both results are equal") { expect(t0).to(equal(t1)) }
                    }
                    context("change text") {
                        let t0 = alter(task,by:.changing(.text(to:"Task 0")))
                        let t1 = task.alter(by:.changing(.text(to:"Task 0")))
                        it("both results are equal") { expect(t0).to(equal(t1)) }
                    }
                    context("change project") {
                        let p = Project(owner:manu,title:"Project 1")
                        let t0 = alter(task,by:.changing(.project(to:p.id)))
                        let t1 = task.alter(by:.changing(.project(to:p.id)))
                        it("both results are equal") { expect(t0).to(equal(t1)) }
                    }
                }
                context("location") {
                    let n = task.alter(by:.changing(.location(to:.coordinate(coord))))
                    it("location is coord"   ) { expect(n.location).to(equal(.coordinate(coord))) }
                    it("project isnt changed") { expect(n.project ).to(equal(project.id)        ) }
                    it("title is unchanged"  ) { expect(n.title   ).to(equal(task.title)        ) }
                    it("task id hasnt change") { expect(n.id      ).to(equal(task.id)           ) }
                    it("text isnt changed"   ) { expect(n.text    ).to(equal(task.text)         ) }
                    it("state isnt changed"  ) { expect(n.state   ).to(equal(task.state)        ) }
                }
                context("state") {
                    context("from unstarted to inprogress") {
                        let n0 = task.alter(by:.changing(.state(to:.unfinished(.inprogress))))
                        it("state changed to finished") { expect(n0.state   ).to(equal(.unfinished(.inprogress))) }
                        it("project isnt changed"     ) { expect(n0.project ).to(equal(project.id)              ) }
                        it("title is unchanged"       ) { expect(n0.title   ).to(equal(task.title)              ) }
                        it("task id hasnt change"     ) { expect(n0.id      ).to(equal(task.id)                 ) }
                        it("text isnt changed"        ) { expect(n0.text    ).to(equal(task.text)               ) }
                        it("location isnt changed"    ) { expect(n0.location).to(equal(project.location)        ) }
                        context("to finished") {
                            let n0 = task.alter(by:.changing(.state(to:.finished)))
                            it("state changed to finished") { expect(n0.state   ).to(equal(.finished)       ) }
                            it("project isnt changed"     ) { expect(n0.project ).to(equal(project.id)      ) }
                            it("title is unchanged"       ) { expect(n0.title   ).to(equal(task.title)      ) }
                            it("task id hasnt change"     ) { expect(n0.id      ).to(equal(task.id)         ) }
                            it("text isnt changed"        ) { expect(n0.text    ).to(equal(task.text)       ) }
                            it("location isnt changed"    ) { expect(n0.location).to(equal(project.location)) }
                            context("and back again") {
                                let n1 = n0.alter(by:.changing(.state(to:.unfinished(.inprogress))))
                                it("state changed to inprogress") { expect(n1.state   ).to(equal(.unfinished(.inprogress))) }
                                it("project isnt changed"       ) { expect(n1.project ).to(equal(n0.project)              ) }
                                it("title is unchanged"         ) { expect(n1.title   ).to(equal(n0.title)                ) }
                                it("task id hasnt change"       ) { expect(n1.id      ).to(equal(n0.id)                   ) }
                                it("text isnt changed"          ) { expect(n1.text    ).to(equal(n0.text)                 ) }
                                it("location isnt changed"      ) { expect(n1.location).to(equal(n0.location)             ) }
                            }
                        }
                        context("and back again") {
                            let n1 = n0.alter(by:.changing(.state(to:.unfinished(.unstarted))))
                            it("state changed to unstarted") { expect(n1.state   ).to(equal(.unfinished(.unstarted))) }
                            it("project isnt changed"      ) { expect(n1.project ).to(equal(n0.project)             ) }
                            it("title is unchanged"        ) { expect(n1.title   ).to(equal(n0.title)               ) }
                            it("task id hasnt change"      ) { expect(n1.id      ).to(equal(n0.id)                  ) }
                            it("text isnt changed"         ) { expect(n1.text    ).to(equal(n0.text)                ) }
                            it("location isnt changed"     ) { expect(n1.location).to(equal(n0.location)            ) }
                        }
                    }
                }
            }
            context("coding") {
                let enc = try! JSONEncoder().encode(task)
                let dec = try! JSONDecoder().decode(Task.self, from:enc)
                context("encoding") {
                    it("task to data"           ) { expect(enc).to(beAKindOf(Data.self)) }
                    it("data has size 214 bytes") { expect(enc).to(haveCount(214)      ) }
                }
                context("decoding") {
                    it("task from data"             ) { expect(dec        ).to(beAKindOf(Task.self)) }
                    it("task same id as before"     ) { expect(dec.id     ).to(equal(task.id)      ) }
                    it("task same title as before"  ) { expect(dec.title  ).to(equal(task.title)   ) }
                    it("task same project as before") { expect(dec.project).to(equal(task.project) ) }
                    it("text isnt changed"          ) { expect(dec.text   ).to(equal(task.text)    ) }
                    it("state isnt changed"         ) { expect(dec.state  ).to(equal(task.state)   ) }
                }
            }
            context("print") {
                it("description") { expect("\(task)").to(equal("Task «\(task.id)» in Project «\(project.id)»: \(task.title)")) }
            }
            context("sets") {
                let setOfTasks = Set([task])
                let n = setOfTasks.first(where:{ $0.id == task.id })!
                it("creates a set of one task") { expect(setOfTasks).to(haveCount(1)       ) }
                it("retrives task by id"      ) { expect(n.id      ).to(equal(task.id)     ) }
                it("project is changed"       ) { expect(n.project ).to(equal(task.project)) }
                it("title is unchanged"       ) { expect(n.title   ).to(equal("task 0")    ) }
                it("task id hasnt change"     ) { expect(n.id      ).to(equal(task.id)     ) }
                it("text isnt changed"        ) { expect(n.text    ).to(equal(task.text)   ) }
                it("state isnt changed"       ) { expect(n.state   ).to(equal(task.state)  ) }
            }
        }
    }
}
