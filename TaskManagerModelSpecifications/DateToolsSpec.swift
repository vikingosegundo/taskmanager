//
//  DateToolsSpec.swift
//  Tools
//
//  Created by Manuel Meyer on 03.07.22.
//

import Foundation
import Quick
import Nimble
import Tools
final class DateToolsSpec:QuickSpec {
    override func spec() {
        let cal = Calendar.autoupdatingCurrent
        describe("DateTools") {
            context("") {
                let comps        = cal.dateComponents([.day,.month,.year],from:Date())
                let today        = cal.date(from:comps)!
                let yesterday    = cal.date(from:DateComponents(year:comps.year!,month:comps.month!,day:comps.day!-1))!
                let aWeekFromNow = cal.date(from:DateComponents(year:comps.year!,month:comps.month!,day:comps.day!+7))!
                let aWeekBefore  = cal.date(from:DateComponents(year:comps.year!,month:comps.month!,day:comps.day!-7))!
                it("today"          ) { expect(today                ).to(beCloseTo(.today    )            ) }
                it("yesterday"      ) { expect(yesterday            ).to(beCloseTo(.yesterday)            ) }
                it("a week from now") { expect(aWeekFromNow.midnight).to(beCloseTo(.aWeekFromNow.midnight)) }
                it("a week before"  ) { expect(aWeekBefore .midnight).to(beCloseTo(.aWeekBefore .midnight)) }
                it("a week earlier" ) { expect(aWeekBefore .noon    ).to(beCloseTo(.aWeekBefore .noon)    ) }
            }
        }
    }
}
