//
//  TodoItemSpec.swift
//  TaskManagerModels
//
//  Created by Manuel Meyer on 02.07.22.
//

import Foundation
import Quick
import Nimble
@testable import TaskManagerModels

final class TodoItemSpec: QuickSpec {
    override func spec() {
        let item = TodoItem(text:"task 0")
        describe("TodoItemSpec") {
            context("created") {
                it("with id"            ) { expect(item.id       ).toNot(beNil()        ) }
                it("with text"          ) { expect(item.text     ).to   (equal("task 0")) }
                it("without duedate"    ) { expect(item.dueDate  ).to   (beNil()        ) }
                it("uncompleted"        ) { expect(item.completed).to   (beFalse()      ) }
                it("no previous version") { expect(item.previous ).to   (beNil()        ) }
            }
            context("replacing"){
                context("completed") {
                    let n = item.alter(by:.setting(.completed(to:true)))
                    it("completed"           ) { expect(n.completed   ).to(beTrue()        ) }
                    it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)  ) }
                    it("text unchanged"      ) { expect(n.text        ).to(equal(item.text)) }
                    it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()         ) }
                    it("has previous version") { expect(n.previous?.id).to(equal(item.id)  ) }
                }
                context("text") {
                    let n = item.alter(by:.setting(.text(to:"replaced")))
                    it("changed text"        ) { expect(n.text        ).to(equal("replaced")    ) }
                    it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)) }
                    it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)       ) }
                    it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()              ) }
                    it("has previous version") { expect(n.previous?.id).to(equal(item.id)       ) }
                }
                context("due date") {
                    let n = item.alter(by:.setting(.dueDate(to:.tomorrow.noon)))
                    it("sets duedate"        ) { expect(n.dueDate     ).to(equal(.tomorrow.noon)) }
                    it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)       ) }
                    it("text unchanged"      ) { expect(n.text        ).to(equal(item.text)     ) }
                    it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)) }
                    it("has previous version") { expect(n.previous?.id).to(equal(item.id)       ) }
                }
            }
            context("modifying") {
                context("text") {
                    context("appending") {
                        let n = item.alter(by:.modifying(.text(by:.appending," o_O")))
                        it("changes text"        ) { expect(n.text        ).to(equal("task 0 o_O")  ) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)       ) }
                        it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()              ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)       ) }
                    }
                    context("prepending") {
                        let n = item.alter(by:.modifying(.text(by:.prepending,"O_o ")))
                        it("changes text"        ) { expect(n.text        ).to(equal("O_o task 0")  ) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)       ) }
                        it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()              ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)       ) }
                    }
                    context("pre & apending") {
                        let n = item.alter(
                            by:
                                .modifying(.text(by:.prepending,"O_o ")),
                                .modifying(.text(by: .appending," o_O"))
                        )
                        it("changes text"        ) { expect(n.text        ).to(equal("O_o task 0 o_O") ) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)          ) }
                        it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()                 ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)   ) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)          ) }
                    }
                    context("double prepending") {
                        let n = item.alter(
                            by:
                                .modifying(.text(by:.prepending,"O_o ")),
                                .modifying(.text(by:.prepending,"o_O "))
                        )
                        it("changes text"        ) { expect(n.text        ).to(equal("o_O O_o task 0") ) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)          ) }
                        it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()                 ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)   ) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)          ) }
                    }
                    context("double appending") {
                        let n = item.alter(
                            by:
                                .modifying(.text(by:.appending," O_o")),
                                .modifying(.text(by:.appending," o_O"))
                        )
                        it("changes text"        ) { expect(n.text        ).to(equal("task 0 O_o o_O") ) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)          ) }
                        it("duedate unchanged"   ) { expect(n.dueDate     ).to(beNil()                 ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)   ) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)          ) }
                    }
                }
                context("postponing") {
                    context("empty dueDate") {
                        context("by a day") {
                            var intercepted:Date!
                            var i:TodoItem!
                            beforeEach { i = item.alter(
                                by:
                                    .execute  ({ intercepted = $0.dueDate ?? Date() }),
                                    .modifying(.dueDate(by:.postponingIt(by:1),.day))) }
                            it("postpones now"       ) { expect(i.dueDate!    ).to(beCloseTo(intercepted.dayAfter,within:0.001)) }
                            it("id unchanged"        ) { expect(i.id          ).to(equal(item.id)                              ) }
                            it("text unchanged"      ) { expect(i.text        ).to(equal(item.text)                            ) }
                            it("completed unchanged" ) { expect(i.completed   ).to(equal(item.completed)                       ) }
                            it("has previous version") { expect(i.previous?.id).to(equal(i.id)                                 ) }
                        }
                        context("by a week") {
                            var intercepted:Date!
                            var i:TodoItem!
                            beforeEach { i = item.alter(
                                by:
                                    .execute({ intercepted = $0.dueDate ?? Date() }),
                                    .modifying(.dueDate(by:.postponingIt(by:1),.week))) }
                            it("postpones now"       ) { expect(i.dueDate!    ).to(beCloseTo(intercepted.aWeekLater,within:0.001)) }
                            it("id unchanged"        ) { expect(i.id          ).to(equal(item.id)                                ) }
                            it("text unchanged"      ) { expect(i.text        ).to(equal(item.text)                              ) }
                            it("completed unchanged" ) { expect(i.completed   ).to(equal(item.completed)                         ) }
                            it("has previous version") { expect(i.previous?.id).to(equal(item.id)                                ) }
                        }
                    }
                }
                context("duedate") {
                    context("by one day") {
                        var intercepted:Date!
                        let n = item.alter(
                            by:
                                .setting(.dueDate(to:.today.noon)),
                                .execute  ({ intercepted = $0.dueDate }),
                                .modifying(.dueDate(by:.postponingIt(by:1),.day)))
                        it("sets duedate"        ) { expect(n.dueDate     ).to(equal(intercepted.dayAfter.noon)) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)                  ) }
                        it("text unchanged"      ) { expect(n.text        ).to(equal(item.text)                ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)           ) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)                  ) }
                    }
                    context("by one week") {
                        var intercepted:Date!
                        let n = item.alter(
                            by:
                                .setting(.dueDate(to:.today.noon)),
                                .execute  ({ intercepted = $0.dueDate }),
                                .modifying(.dueDate(by:.postponingIt(by:1),.week)))
                        it("sets duedate"        ) { expect(n.dueDate     ).to(equal(intercepted.aWeekLater)) }
                        it("id unchanged"        ) { expect(n.id          ).to(equal(item.id)               ) }
                        it("text unchanged"      ) { expect(n.text        ).to(equal(item.text)             ) }
                        it("completed unchanged" ) { expect(n.completed   ).to(equal(item.completed)        ) }
                        it("has previous version") { expect(n.previous?.id).to(equal(item.id)               ) }
                    }
                }
            }
            context("forking") {
                context("without changes") {
                    let n = item.alter(by:.forking([]))
                    it("new id"              ) { expect(n.id          ).toNot(equal(item.id)       ) }
                    it("unchanged text"      ) { expect(n.text        ).to   (equal(item.text)     ) }
                    it("duedate unchanged"   ) { expect(n.dueDate     ).to   (beNil()              ) }
                    it("completed unchanged" ) { expect(n.completed   ).to   (equal(item.completed)) }
                    it("has previous version") { expect(n.previous?.id).to   (equal(item.id)       ) }
                }
                context("with new text") {
                    let n = item.alter(by:.forking([.setting(.text(to:"task 1"))]))
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)       ) }
                    it("changed text"                  ) { expect(n.text        ).to   (equal("task 1")      ) }
                    it("duedate unchanged"             ) { expect(n.dueDate     ).to   (beNil()              ) }
                    it("completed unchanged"           ) { expect(n.completed   ).to   (equal(item.completed)) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)       ) }
                }
                context("with new text and duedate") {
                    var d:Date!
                    var n:TodoItem!
                    beforeEach {
                        n = item.alter(
                            by:
                                .forking([
                                    .setting(.text(to:"task 1")),
                                    .setting(.dueDate(to:Date())),
                                    .execute { d = $0.dueDate },
                                    .modifying(.dueDate(by:.postponingIt(by:1),.day))]))
                    }
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)        ) }
                    it("has a dueDate"                 ) { expect(n.dueDate     ).toNot(beNil()               ) }
                    it("changed text"                  ) { expect(n.text        ).to   (equal("task 1")       ) }
                    it("duedate changed"               ) { expect(n.dueDate     ).to   (equal(d.dayAfter)     ) }
                    it("completed unchanged"           ) { expect(n.completed   ).to   (equal(item.completed) ) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)        ) }
                }
                context("with new due date") {
                    let n = item.alter(by:.forking([.setting(.dueDate(to:.tomorrow.noon))]))
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)       ) }
                    it("duedate changed"               ) { expect(n.dueDate     ).to   (equal(.tomorrow.noon)) }
                    it("unchanged text"                ) { expect(n.text        ).to   (equal(item.text)     ) }
                    it("completed unchanged"           ) { expect(n.completed   ).to   (equal(item.completed)) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)       ) }
                }
                context("completed") {
                    let n = item.alter(by:.forking([.setting(.completed(to:true))]))
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)       ) }
                    it("completed changed"             ) { expect(n.completed   ).toNot(equal(item.completed)) }
                    it("duedate unchanged"             ) { expect(n.dueDate     ).to   (beNil()              ) }
                    it("unchanged text"                ) { expect(n.text        ).to   (equal(item.text)     ) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)       ) }
                }
                context("with appending text") {
                    let n = item.alter(by:.forking([.modifying(.text(by:.appending," :)"))]))
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)       ) }
                    it("changed text"                  ) { expect(n.text        ).to   (equal("task 0 :)")   ) }
                    it("duedate unchanged"             ) { expect(n.dueDate     ).to   (beNil()              ) }
                    it("completed unchanged"           ) { expect(n.completed   ).to   (equal(item.completed)) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)       ) }
                }
                context("with prepending text") {
                    let n = item.alter(by:.forking([.modifying(.text(by:.prepending,"(: "))]))
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)       ) }
                    it("changed text"                  ) { expect(n.text        ).to   (equal("(: task 0")   ) }
                    it("duedate unchanged"             ) { expect(n.dueDate     ).to   (beNil()              ) }
                    it("completed unchanged"           ) { expect(n.completed   ).to   (equal(item.completed)) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)       ) }
                }
                context("with new text and duedate") {
                    var n:TodoItem!
                    beforeEach {
                        n = item.alter(
                            by:
                                .forking([
                                    .setting  (.text(to:"task 1")),
                                    .modifying(.dueDate(by:.postponingIt(by:1),.day)),
                                    .modifying(.text(by:.appending," :D")),
                                    .setting  (.completed(to:true))]))
                    }
                    it("new id"                        ) { expect(n.id          ).toNot(equal(item.id)                     ) }
                    it("changed text"                  ) { expect(n.text        ).to   (equal("task 1 :D")                 ) }
                    it("duedate changed"               ) { expect(n.dueDate     ).to   (beCloseTo(.aDayFromNow,within:0.01)) }
                    it("completed changed"             ) { expect(n.completed   ).to   (beTrue()                           ) }
                    it("has different previous version") { expect(n.previous?.id).toNot(equal(item.id)                     ) }
                }
            }
            context("execute"){
                context("cutpoints") {
                    var d:(Date?,Date?) = (nil,nil)
                    _ = item.alter(
                        by:
                            .setting  ( .dueDate(to:.tomorrow.noon)           ),
                            .execute  ( { d.0 = $0.dueDate }                  ),
                            .modifying( .dueDate(by:.postponingIt(by:1),.day) ),
                            .execute  ( { d.1 = $0.dueDate }                  ))
                    it("will caputure different values for different executes"){ expect((d.1!.timeIntervalSince1970 - d.0!.timeIntervalSince1970) / 3600).to(beWithin(23...25))}
                }
                context("execute invariant") {
                    var i:(TodoItem?,TodoItem?) = (nil,nil)
                    _ = item.alter(
                        by:
                            .setting  ( .dueDate(to:.tomorrow.noon) ),
                            .execute  ( { i.0 = $0 }                ),
                            .execute  ( { i.1 = $0 }                ))
                    context("does not changes object") {
                        it("completed"           ) { expect(i.0!.completed   ).to(equal(i.1!.completed   )) }
                        it("id unchanged"        ) { expect(i.0!.id          ).to(equal(i.1!.id          )) }
                        it("text unchanged"      ) { expect(i.0!.text        ).to(equal(i.1!.text        )) }
                        it("duedate changed"     ) { expect(i.0!.dueDate     ).to(equal(i.1!.dueDate)) }
                        it("has previous version") { expect(i.0!.previous?.id).to(equal(i.1!.previous?.id)) }
                    }
                    context("does not change history") {
                        it("completed"           ) { expect(i.0!.previous!.completed).to(equal(i.1!.previous!.completed   )) }
                        it("id unchanged"        ) { expect(i.0!.previous!.id       ).to(equal(i.1!.previous!.id          )) }
                        it("text unchanged"      ) { expect(i.0!.previous!.text     ).to(equal(i.1!.previous!.text        )) }
                        it("duedate unchanged"   ) { expect(i.0!.previous!.dueDate  ).to(beNil()) }
                    }
                }
            }
            context("description") {
                context("as created") {
                    let i = item
                    it("") { expect(i.description).to(equal("task 0\t𐄂 \tcreated:\(i.created)\n")) }
                }
                context("with due date") {
                    var d,c:String!
                    let i = item.alter(
                        by:
                            .setting(.dueDate(to:.tomorrow.noon)    ),
                            .execute({ d = $0.dueDate!.description }),
                            .execute({ c = $0.created .description }))
                    it("") { expect(i.description).to(equal("task 0\t𐄂 \tdue:\(d!)\tcreated:\(c!)\n\(item)")) }
                }
                context("completed") {
                    let i = item.alter(by:.setting(.completed(to:true)))
                    let s = i.description
                    it("") { expect(s).to(equal("task 0\t✓ \tcreated:\(i.created)\n\(item)")) }
                }
            }
        }
    }
}
