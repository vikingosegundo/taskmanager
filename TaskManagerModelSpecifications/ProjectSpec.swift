//
//  ProjectSpec.swift
//  TaskManagerModelsTests
//
//  Created by Manuel Meyer on 26.06.22.
//

import Quick
import Nimble
@testable import TaskManagerModels

fileprivate let manu    = Collaborator(name:"Manu" )
fileprivate let ernie   = Collaborator(name:"Ernie")
fileprivate let bert    = Collaborator(name:"Bert" )
fileprivate let otto    = Collaborator(name:"otto" )
fileprivate let project = Project(  owner:manu   ,title:"project"  )
fileprivate let task    = Task   (project:project.id,title:"task 0"   )
fileprivate let subtask = SubTask(   task:task   ,title:"subtask 0")
fileprivate let address = Location.Address(street:"street", city:"city")
fileprivate let coord   = Location.Coordinate(latitude:53.2190827, longitude:6.5773836)

final class ProjectSpec: QuickSpec {
    override func spec() {
        describe("Project") {
            context("creation") {
                it("is created with an id"                   ) { expect(project.id           ).toNot(beNil()             ) }
                it("is created with title"                   ) { expect(project.title        ).to   (equal(project.title)) }
                it("is created with empty text"              ) { expect(project.text         ).to   (beEmpty()           ) }
                it("is created with an owner"                ) { expect(project.owner        ).to   (equal( manu.id)     ) }
                it("is created with an owner as collaborator") { expect(project.collaborators).to   (equal([manu.id])    ) }
                it("is created without given location"       ) { expect(project.location     ).to   (equal(.unknown)     ) }
            }
            context("alteration") {
                context("title") {
                    let n = alter(project, by:.changing(.title(to:"project title")))
                    it("changes title"              ) { expect(n.title        ).to(equal("project title") ) }
                    it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)    ) }
                    it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)      ) }
                    it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)   ) }
                    it("doesnt change collaborators") { expect(n.collaborators).to(equal([project.owner]) ) }
                    it("doesnt change location"     ) { expect(n.location     ).to(equal(project.location)) }
                }
                context("text") {
                    let n = alter(project, by:.changing(.text(to:"altered text")))
                    it("changes text"               ) { expect(n.text         ).to(equal("altered text")  ) }
                    it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)   ) }
                    it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)      ) }
                    it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)   ) }
                    it("doesnt change collaborators") { expect(n.collaborators).to(equal([project.owner]) ) }
                    it("doesnt change location"     ) { expect(n.location     ).to(equal(project.location)) }
                }
                context("owner") {
                    let n = alter(project, by:.changing(.owner(to:otto.id)))
                    it("changes owner"              ) { expect(n.owner        ).to(equal(otto.id)              ) }
                    it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                    it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                    it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                    it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    it("doesnt change location"     ) { expect(n.location     ).to(equal(project.location)     ) }
                }
                context("location") {
                    context("unknown to address") {
                        let n = alter(project, by:.changing(.location(to:.address(address))))
                        it("change location to address" ) { expect(n.location     ).to(equal(.address(address))    ) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                    context("unknown to coordinate") {
                        let n = project
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                        it("change location to coord"   ) { expect(n.location     ).to(equal(.coordinate(coord))   ) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                    context("unknown to textual") {
                        let n = project
                            .alter(by:.changing(.location(to:.textual("end of street"))))
                        it("change location to coord"   ) { expect(n.location     ).to(equal(.textual("end of street"))) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)            ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)               ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)            ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)             ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)    ) }
                    }
                    context("coordinate to unknown") {
                        let n = project
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                            .alter(by:.changing(.location(to:.unknown)))
                        it("change location to unknown" ) { expect(n.location     ).to(equal(.unknown)             ) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                    context("coordinate to textual") {
                        let n = project
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                            .alter(by:.changing(.location(to:.textual("on the crossing"))))
                        it("change location to textual" ) { expect(n.location     ).to(equal(.textual("on the crossing"))) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)              ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)                 ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)              ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)               ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)      ) }
                    }
                    context("address to unknown") {
                        let n = project
                            .alter(by:.changing(.location(to:.address(address))))
                            .alter(by:.changing(.location(to:.unknown)))
                        it("change location to unknown" ) { expect(n.location     ).to(equal(.unknown)             ) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                    context("address to textual") {
                        let n = project
                            .alter(by:.changing(.location(to:.address(address))))
                            .alter(by:.changing(.location(to:.textual("at the fire hydrant"))))
                        it("change location to textual" ) { expect(n.location     ).to(equal(.textual("at the fire hydrant"))) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)                  ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)                     ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)                  ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)                   ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)          ) }
                    }
                    context("textual to unknown") {
                        let n = project
                            .alter(by:.changing(.location(to:.textual("playground"))))
                            .alter(by:.changing(.location(to:.unknown)))
                        it("change location to unknown" ) { expect(n.location     ).to(equal(.unknown)             ) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                    context("textual to textual") {
                        let n = project
                            .alter(by:.changing(.location(to:.textual("playground"))))
                            .alter(by:.changing(.location(to:.textual("next to the fire hydrant"))))
                        it("change location to textual" ) { expect(n.location     ).to(equal(.textual("next to the fire hydrant"))) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)                       ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)                          ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)                       ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)                        ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)               ) }
                    }
                    context("address to altered address") {
                        let alteredAddress = alter(address, by:.changing(.street(to:"BN 32")))
                        let n = project
                            .alter(by:.changing(.location(to:.address(address))))
                            .alter(by:.changing(.location(to:.address(
                                alter(address, by:.changing(.street(to:"BN 32")))))))
                        it("change location to new address") { expect(n.location     ).to(equal(.address(alteredAddress))) }
                        it("doesnt change owner"           ) { expect(n.owner        ).to(equal(project.owner)           ) }
                        it("doesnt change id"              ) { expect(n.id           ).to(equal(project.id)              ) }
                        it("doesnt change title"           ) { expect(n.title        ).to(equal(project.title)           ) }
                        it("doesnt change text"            ) { expect(n.text         ).to(equal(project.text)            ) }
                        it("doesnt change collaborators"   ) { expect(n.collaborators).to(equal(project.collaborators)   ) }
                    }
                    context("address to coordinate") {
                        let n = project
                            .alter(by:.changing(.location(to:.address(address))))
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                        it("change location to coordinate") { expect(n.location     ).to(equal(.coordinate(coord))   ) }
                        it("doesnt change owner"          ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"             ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"          ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"           ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators"  ) { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                    context("coordinate to address") {
                        let n = project
                            .alter(by:.changing(.location(to:.coordinate(coord))))
                            .alter(by:.changing(.location(to:.address(address))))
                        it("change location to address" ) { expect(n.location     ).to(equal(.address(address))    ) }
                        it("doesnt change owner"        ) { expect(n.owner        ).to(equal(project.owner)        ) }
                        it("doesnt change id"           ) { expect(n.id           ).to(equal(project.id)           ) }
                        it("doesnt change title"        ) { expect(n.title        ).to(equal(project.title)        ) }
                        it("doesnt change text"         ) { expect(n.text         ).to(equal(project.text)         ) }
                        it("doesnt change collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    }
                }
                context("add collaborator") {
                    let n = project.alter(
                        by:.adding(.collaborator(ernie.id)))
                    it("change collaborators"  ) { expect(n.collaborators).to(equal([n.owner,ernie.id])) }
                    it("doesnt changes owner"  ) { expect(n.owner        ).to(equal(project.owner)     ) }
                    it("doesnt change id"      ) { expect(n.id           ).to(equal(project.id)        ) }
                    it("doesnt change title"   ) { expect(n.title        ).to(equal(project.title)     ) }
                    it("doesnt change text"    ) { expect(n.text         ).to(equal(project.text)      ) }
                    it("doesnt change location") { expect(n.location     ).to(equal(project.location)  ) }
                }
                context("add three collaborators") {
                    let n = project.alter(
                        by:
                            .adding(.collaborator(ernie.id)),
                            .adding(.collaborator(otto.id)),
                            .adding(.collaborator(bert.id)))
                    it("change collaborators"  ) { expect(n.collaborators).to(equal([n.owner,ernie.id,otto.id,bert.id])) }
                    it("doesnt changes owner"  ) { expect(n.owner        ).to(equal(project.owner)                     ) }
                    it("doesnt change id"      ) { expect(n.id           ).to(equal(project.id)                        ) }
                    it("doesnt change title"   ) { expect(n.title        ).to(equal(project.title)                     ) }
                    it("doesnt change text"    ) { expect(n.text         ).to(equal(project.text)                      ) }
                    it("doesnt change location") { expect(n.location     ).to(equal(project.location)                  ) }
                }
                context("add same collaborator twice") {
                    let n = project.alter(
                        by:
                            .adding(.collaborator(ernie.id)),
                            .adding(.collaborator(ernie.id)))
                    it("wont add it twice"     ) { expect(n.collaborators).to(equal([n.owner,ernie.id])) }
                    it("doesnt changes owner"  ) { expect(n.owner        ).to(equal(project.owner)     ) }
                    it("doesnt change id"      ) { expect(n.id           ).to(equal(project.id)        ) }
                    it("doesnt change title"   ) { expect(n.title        ).to(equal(project.title)     ) }
                    it("doesnt change text"    ) { expect(n.text         ).to(equal(project.text)      ) }
                    it("doesnt change location") { expect(n.location     ).to(equal(project.location)  ) }
                }
                context("remove collaborator") {
                    let n = project
                        .alter(
                            by:
                                .adding(.collaborator(ernie.id)),
                                .adding(.collaborator(bert .id)))
                        .alter(
                            by:.removing(.collaborator(ernie.id)))
                    it("change collaborators"  ) { expect(n.collaborators).to(equal([n.owner,bert.id])) }
                    it("doesnt changes owner"  ) { expect(n.owner        ).to(equal(project.owner)    ) }
                    it("doesnt change id"      ) { expect(n.id           ).to(equal(project.id)       ) }
                    it("doesnt change title"   ) { expect(n.title        ).to(equal(project.title)    ) }
                    it("doesnt change text"    ) { expect(n.text         ).to(equal(project.text)     ) }
                    it("doesnt change location") { expect(n.location     ).to(equal(project.location) ) }
                }
                context("remove non-existing collaborator") {
                    let n = project
                        .alter(
                            by:.removing(.collaborator(ernie.id)))
                    it("doesnt changes owner"   ) { expect(n.owner        ).to(equal(project.owner)        ) }
                    it("doesnt change id"       ) { expect(n.id           ).to(equal(project.id)           ) }
                    it("doesnt change title"    ) { expect(n.title        ).to(equal(project.title)        ) }
                    it("doesnt change text"     ) { expect(n.text         ).to(equal(project.text)         ) }
                    it("unchanged collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    it("doesnt change location" ) { expect(n.location     ).to(equal(project.location)     ) }
                }
                context("cant remove owner as collaborator") {
                    let n = project
                        .alter(
                            by:.removing(.collaborator(project.owner)))
                    it("unchanged collaborators") { expect(n.collaborators).to(equal(project.collaborators)) }
                    it("doesnt changes owner"   ) { expect(n.owner        ).to(equal(project.owner)        ) }
                    it("doesnt change id"       ) { expect(n.id           ).to(equal(project.id)           ) }
                    it("doesnt change title"    ) { expect(n.title        ).to(equal(project.title)        ) }
                    it("doesnt change text"     ) { expect(n.text         ).to(equal(project.text)         ) }
                    it("doesnt change location" ) { expect(n.location     ).to(equal(project.location)     ) }
                }
            }
            context("coding") {
                let enc = try! JSONEncoder().encode(project)
                let dec = try! JSONDecoder().decode(Project.self, from: enc)
                context("encoding") {
                    it("project to data") { expect(enc).to(beAKindOf(Data.self)) }
                }
                context("decoding") {
                    it("project from data"                  ) { expect(dec              ).to(beAKindOf(Project.self)     ) }
                    it("project same id as before"          ) { expect(dec.id           ).to(equal(project.id)           ) }
                    it("project same title as before"       ) { expect(dec.title        ).to(equal(project.title)        ) }
                    it("project same text as before"        ) { expect(dec.text         ).to(equal(project.text)         ) }
                    it("project same owner as before"       ) { expect(dec.owner        ).to(equal(project.owner)        ) }
                    it("project has collaborators as before") { expect(dec.collaborators).to(equal(project.collaborators)) }
                    it("doesnt change location"             ) { expect(dec.location     ).to(equal(project.location)     ) }
                }
            }
            context("print") {
                it("description") { expect("\(project)").to(equal("Project «\(project.id)»: \(project.title)")) }
            }
            context("sets") {
                let setOfProjects = Set([project])
                let n = setOfProjects.first(where:{ $0.id == project.id })!
                it("creates a set of one project"       ) { expect(setOfProjects  ).to(haveCount(1)                ) }
                it("retrives project by id"             ) { expect(n.id           ).to(equal(project.id)           ) }
                it("project same id as before"          ) { expect(n.id           ).to(equal(project.id)           ) }
                it("project same title as before"       ) { expect(n.title        ).to(equal(project.title)        ) }
                it("project same text as before"        ) { expect(n.text         ).to(equal(project.text )        ) }
                it("project same owner as before"       ) { expect(n.owner        ).to(equal(project.owner)        ) }
                it("project has collaborators as before") { expect(n.collaborators).to(equal(project.collaborators)) }
                it("doesnt change location"             ) { expect(n.location     ).to(equal(project.location)     ) }
            }
        }
    }
}
