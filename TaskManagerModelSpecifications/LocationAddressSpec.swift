//
//  LocationAddressSpec.swift
//  TaskManagerModelsTests
//
//  Created by Manuel Meyer on 26.06.22.
//

import Quick
import Nimble
@testable import TaskManagerModels

fileprivate let manu    = Collaborator(name:"Manu" )
fileprivate let project = Project(  owner:manu   ,title:"project"  )
fileprivate let task    = Task   (project:project.id,title:"task 0"   )
fileprivate let subtask = SubTask(   task:task   ,title:"subtask 0")
fileprivate let address = Location.Address(street:"street", city:"city")
fileprivate let coord   = Location.Coordinate(latitude:53.2190827, longitude:6.5773836)

final class LocationAddressSpec: QuickSpec {
    override func spec() {
        describe("Location.Address") {
            context("creation") {
                it("is ctreated with city"  ) { expect(address.city  ) == "city"   }
                it("is ctreated with street") { expect(address.street) == "street" }
            }
            context("alteration") {
                context("change city") {
                    let a = alter(address,by:.changing(.city(to:"city b")))
                    it("changes city name"                 ) { expect(a.city  ) == "city b"       }
                    it("city name not same in both objects") { expect(a.city  ) != address.city   }
                    it("doesnt change street name"         ) { expect(a.street) == "street"       }
                    it("street name same in both objects"  ) { expect(a.street) == address.street }
                }
                context("change street") {
                    let a = alter(address,by:.changing(.street(to:"street b")))
                    it("changes street name"                 ) { expect(a.street) == "street b"     }
                    it("street name not same in both objects") { expect(a.street) != address.street }
                    it("doesnt change city name       "      ) { expect(a.city  ) == "city"         }
                    it("city name same in both objects"      ) { expect(a.city  ) == address.city   }
                }
                context("alter function and method are equivalent") {
                    let a0 = address.alter(by:.changing(.city(to:"city b")))
                    let a1 = alter(address,by:.changing(.city(to:"city b")))
                    it("addresses are equal") { expect(a0).to(equal(a1)) }
                }
            }
        }
    }
}
