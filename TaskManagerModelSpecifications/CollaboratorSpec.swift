//
//  CollaboratorSpec.swift
//  TaskManagerModelsTests
//
//  Created by Manuel Meyer on 26.06.22.
//
import Quick
import Nimble
@testable import TaskManagerModels

fileprivate let manu = Collaborator(name:"Manu" )

final class CollaboratorSpec: QuickSpec {
    override func spec() {
        describe("Collaborator") {
            context("creation") {
                it("is created with an id") { expect(manu.id  ).toNot(beNil()      ) }
                it("is created with name" ) { expect(manu.name).to   (equal("Manu")) }
            }
            context("alteration") {
                context("name") {
                    let n = manu.alter(.name("vikingosegundo"))
                    it("doesnt change id") { expect(n.id  ).to(equal(manu.id)         ) }
                    it("changes name"    ) { expect(n.name).to(equal("vikingosegundo")) }
                }
            }
            context("print") {
                it("description") { expect("\(manu)").to(equal("Collaborator «\(manu.id)»: \(manu.name)")) }
            }
        }
    }
}
