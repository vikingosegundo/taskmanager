//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import TaskManagerModels

public final class ViewState: ObservableObject  {
    @Published public var projects:[Project] = []
    @Published public var collaborators: [Collaborator] = []
    public var store:Store
    public var execute:(Message) -> () = { _ in }
    public init(store: Store) {
        self.store = store
        store.updated { self.process(store.state()) }
        process(store.state())
    }
}

extension ViewState {
    private func process(_ appState:AppState) {
        self.projects = appState.projects.reversed()
            self.collaborators = appState.collaborators
    }
}
