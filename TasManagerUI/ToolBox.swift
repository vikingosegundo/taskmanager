//
//  ToolPicker.swift
//  TaskManagerUI
//
//  Created by Manuel Meyer on 09.07.22.
//

import SwiftUI
import TaskManagerModels

public enum Tool:Equatable {
    case none
    case collaborators(_Collaborators)
    case projects(_Projects)
    case todos(_Todos)
    public enum _Collaborators:Equatable {
        case add(Collaborator)
        case search(by:_Search)
        public enum _Search:Equatable {
            case name(String)
        }
    }
    public enum _Projects:Equatable {
        case add(Project)
        case display(Project)
        case search
    }
    public enum _Todos:Equatable {
        case add
        case search
    }
}
public struct ToolBox: View {
    enum Drawer {
        case collaborators
        case projects
        case todos
    }
    @Binding private var selected: Tool
    @EnvironmentObject var viewState: ViewState

    let drawer:Drawer
    init(drawer d:Drawer, selection s:Binding<Tool>) { drawer = d; _selected = s }
    public var body: some View {
        VStack {
            HStack {
                switch drawer {
                case .collaborators:
                    HStack {
                        Button { selected = .collaborators(.add(Collaborator(name:""))) } label: {
                            VStack {
                                Label("add collaborator", systemImage: "person.fill.badge.plus")
                                    .foregroundColor(.black)
                                    .padding(0)
                            }
                        }
                        Button { selected = .collaborators(.search(by:.name(""))) } label: {
                            VStack {
                                Image(systemName:"person.fill.questionmark")
                                    .foregroundColor(.black)
                                    .padding(0)
                                if #available(iOS 16.0, *) {
                                    Text("search collaborators")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .padding(.init(top:3,leading:0,bottom:3,trailing:0))
                                        .underline(isCollaboratorSearchTool(selected:selected))
                                } else {
                                    Text("search collaborators")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .padding(.init(top:3,leading:0,bottom:3,trailing:0))                                }
                            }
                        }
                    }
                case .projects:
                        HStack {
                            Button { selected = .projects(.display(projectFromDisplayTool(selected,owner:viewState.collaborators.randomElement()!))) } label: {
                                VStack {
                                    Image(systemName:"doc.plaintext")
                                        .foregroundColor(.black)
                                        .background(isProjectDisplayTool(selected:selected) ? .white : .clear)
                                        .padding(1)
                                        .padding(.top,3)
                                    Text("inspect project")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .background(isProjectDisplayTool(selected:selected) ? .white : .clear)
                                        .padding(1)
                                }
                                .patterened(with:isProjectDisplayTool(selected:selected) ? .dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal) : .none)
                                .frame(width:84,height:54)
                                .border(isProjectDisplayTool(selected:selected) ? .gray : .clear,width:1)
                                .cornerRadius(4)
                            }
                            Button { selected = .projects(.add(projectFromAddTool(selected,owner:viewState.collaborators.randomElement()!))) } label: {
                                VStack {
                                    Image(systemName:"doc.badge.plus")
                                        .foregroundColor(.black)
                                        .background(isProjectAddTool(selected:selected) ? .white : .clear)
                                        .padding(1)
                                        .padding(.top,3)
                                    Text("add project")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .background(isProjectAddTool(selected:selected) ? .white : .clear)
                                        .padding(1)
                                }
                                .patterened(with:isProjectAddTool(selected:selected) ? .dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal) : .none)
                                .frame(width:74,height:54)
                                .border(isProjectAddTool(selected:selected) ? .gray : .clear,width:1)
                                .cornerRadius(4)
                            }
                            Button { selected = .projects(.search) } label: {
                                VStack {
                                    Image(systemName:"doc.text.magnifyingglass")
                                        .foregroundColor(.black)
                                        .background(isProjectSearchTool(selected:selected) ? .white : .clear)
                                        .padding(1)
                                        .padding(.top,3)
                                    Text("search projects")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .background(isProjectSearchTool(selected:selected) ? .white : .clear)
                                        .padding(1)
                                }
                                .patterened(with:isProjectSearchTool(selected:selected) ? .dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal) : .none)
                                .frame(width:84,height:54)
                                .border(isProjectSearchTool(selected:selected) ? .gray : .clear,width:1)
                                .cornerRadius(4)
                            }
                        }.frame(maxWidth:.infinity,alignment:.center)
                case .todos:
                    HStack {
                        Button { selected = .todos(.add   ) } label: { Text("add todo") }
                        Button { selected = .todos(.search) } label: { Text("search todos") }
                    }
                }
            }
            Spacer()
        }
    }
}
//MARK: - private
fileprivate func isCollaboratorSearchTool(selected t:Tool) -> Bool {
    switch t {
    case .collaborators(.search): return true
    default: return false
    }
}
fileprivate func isCollaboratorAddTool(selected t:Tool) -> Bool {
    switch t {
    case .collaborators(.add): return true
    default: return false
    }
}
fileprivate func isProjectAddTool(selected t:Tool) -> Bool {
    switch t {
    case .projects(.add): return true
    default: return false
    }
}
fileprivate func isProjectDisplayTool(selected t:Tool) -> Bool {
    switch t {
    case .projects(.display): return true
    default: return false
    }
}
fileprivate func projectFromAddTool(_ tool:Tool, owner:Collaborator) -> Project {
    switch tool {
    case .projects(.add(let p)): return p
    default: return Project(owner:owner,title:"")
    }
}
fileprivate func projectFromDisplayTool(_ tool:Tool, owner:Collaborator) -> Project {
    switch tool {
    case .projects(.display(let p)): return p
    default: return Project(owner:owner,title:"")
    }
}
fileprivate func isProjectSearchTool(selected t:Tool) -> Bool {
    switch t {
    case .projects(.search): return true
    default: return false
    }
}
