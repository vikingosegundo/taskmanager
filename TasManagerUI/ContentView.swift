//
//  ContentView.swift
//  TaskManager
//
//  Created by Manuel Meyer on 14.06.22.
//

import SwiftUI
import TaskManagerModels

public struct ContentView {
    public init(store:Store, rootHandler: @escaping (Message) -> ()) {
        execute = rootHandler
        _viewState = StateObject(wrappedValue:ViewState(store:store))
        self.tabs = [
            .init(icon: Image(systemName:"person.3.fill"),title:"Collaborators",color:.init(white:0.6)),
            .init(icon: Image(systemName:"film.fill"    ),title:"Projects"     ,color:.init(white:0.5)),
            .init(icon: Image(systemName:"book.fill"    ),title:"Todos"        ,color:.init(white:0.4))
        ]
    }
    @StateObject private var viewState  : ViewState
    @State       private var selectedTab: Int = 1
    
    @State       private var collaboratorsTool: Tool = .none
    @State       private var      projectsTool: Tool = .none
    @State       private var         todosTool: Tool = .none
    
    @State       private var openDrawer      : ToolBox.Drawer = .projects
    @State       private var collaboratorName: String         = ""
    @State       private var projectTitle    : String         = ""
    @State       private var taskTitle       : String         = ""
    
    private let execute: (Message) -> ()
    private let tabs   : [Tab]
}
extension ContentView: View {
    public var body: some View {
        GeometryReader { geo in
            VStack {
                VStack(spacing:0) {
                    VStack(spacing:0) {
                        switch openDrawer {
                        case .collaborators: listOfCollaborators()
                        case .projects     : listOfProjects()
                        case .todos        : listOfTodos()
                        }
                    }
                    Spacer()
                    VStack(spacing:0) {
                        TabView(selection:$selectedTab) {
                            VStack { collaboratorPanel() }.tag(0)
                            VStack { projectsPanel()     }.tag(1)
                            VStack { todosPanel()        }.tag(2)
                        }
                        .tabViewStyle(PageTabViewStyle(indexDisplayMode:.never))
                        Tabs(tabs:tabs,geoWidth:geo.size.width,selectedTab:$selectedTab)
                    }.onChange(of:selectedTab) {
                        switch $0 {
                        case 0: openDrawer = .collaborators
                        case 1: openDrawer = .projects
                        case 2: openDrawer = .todos
                        default: ()
                        }
                    }
                    .frame(height:282)
                    .frame(alignment:.bottom)
                }
            }
        }
        .environmentObject(viewState)
        .onChange(of:selectedTab) {
            switch $0 {
            case 0: openDrawer = .collaborators
            case 1: openDrawer = .projects
            case 2: openDrawer = .todos
            default: ()
            }
        }
    }
}
//MARK: - private
extension ContentView {
    @ViewBuilder
    private func collaboratorPanel() -> some View {
        Panel(   title: "Collaborators",
                 selection: $collaboratorsTool,
                 height: 200,
                 secondaryColor: tabs[0].color)
        {
            HStack(spacing:0) {
                VStack(spacing:0) {
                    VStack(spacing:4) {
                        ScrollView(showsIndicators:false) {
                            ForEach(
                                Array([// selected image            unselected image            button title
                                    (("person.fill.badge.plus"  , "person.badge.plus"       ), "add collaborator"    ),
                                    (("person.fill.questionmark", "person.fill.questionmark"), "search collaborators")
                                      ].enumerated()), id:\.0) { x in // (index, ((selected, unselected), title))
                                          let idx = x.0
                                          let image = (selected:x.1.0.0,unselected:x.1.0.1)
                                          let title = x.1.1
                                          Button {
                                              switch x.0 {
                                              case 0:  collaboratorsTool = .collaborators(.add(.init(name:"")))
                                              case 1:  collaboratorsTool = .collaborators(.search(by:.name("")))
                                              default: collaboratorsTool = .none
                                              }
                                          } label: {
                                              VStack {
                                                  Image(systemName:
                                                            (idx == 0 && isCollaboratorsAddTool  (selected: collaboratorsTool)) ||
                                                        (idx == 1 && isCollaboratorSearchTool(selected: collaboratorsTool))
                                                        ? image.selected : image.unselected)
                                                  .foregroundColor(.black)
                                                  .background(((idx == 0 && isCollaboratorsAddTool  (selected: collaboratorsTool)) ||
                                                               (idx == 1 && isCollaboratorSearchTool(selected: collaboratorsTool))
                                                               ? .white :.clear))
                                                  .padding(1)
                                                  .padding(.top,3)
                                                  Text(title)
                                                      .font(.caption2)
                                                      .foregroundColor(.black)
                                                      .background(.white)
                                                      .padding(1)
                                              }
                                              .patterened(with:((idx == 0 && isCollaboratorsAddTool  (selected: collaboratorsTool)) ||
                                                                (idx == 1 && isCollaboratorSearchTool(selected: collaboratorsTool))
                                                                ?  .dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal) : .none))
                                              .frame(width:84,height:54)
                                              .border(((idx == 0 && isCollaboratorsAddTool(selected:collaboratorsTool)) ||
                                                       (idx == 1 && isCollaboratorSearchTool(selected:collaboratorsTool)))
                                                      ?  .gray : .clear,width:1)
                                              .cornerRadius(4)
                                          }
                                      }
                        }
                        
                    }.frame(width:98,alignment:.center)
                        .padding(.leading,2)
                        .padding(.top,2)
                        .padding(.trailing,2)
                }.frame(alignment:.leading)
                VStack {
                    drawer(tool:collaboratorsTool) { t in
                        Group {
                            switch t {
                            case .collaborators(.add):
                                VStack {
                                    VStack {
                                        HStack {
                                            TextField("Enter name",text:$collaboratorName)
                                                .textFieldStyle(RectangleTextFieldStyle())
                                                .modifier(ClearButton(text:$collaboratorName,color:.black.opacity(0.65)))
                                                .padding(.trailing,12)
                                        }
                                        HStack {
                                            HStack {
                                                Button {
                                                    execute(.taskmanager(.add(.collaborator(.init(name:collaboratorName)))))
                                                    collaboratorsTool = .collaborators(.add(.init(name:"")))
                                                    collaboratorName = ""
                                                } label: {
                                                    VStack {
                                                        Text("Add Collaborator")
                                                            .dynamicTypeSize(.xSmall)
                                                            .background(.white)
                                                            .shadow(color:.black.opacity(0.3),radius:1)
                                                    }
                                                    .patterened(with:.dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal))
                                                    .frame(width:140, height:44)
                                                    .border(.gray)
                                                    .cornerRadius(4)
                                                }
                                                .foregroundColor(.black)
                                                .tint(.black)
                                                .disabled(collaboratorName.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty)
                                                .frame(height:44)
                                            }
                                        }
                                    }
                                }
                            case .projects(.display):
                                if openDrawer == .projects && isProjectDisplayTool(selected:projectsTool) {
                                    if let p = projectFromDisplayTool(projectsTool) {
                                        if !viewState.projects.contains(p) {
                                            Text("select a project from the list above")
                                        } else {
                                            VStack {
                                                TextField("task title", text:$taskTitle)
                                                    .patterened(with:.dotted(cellSize:2,dotSize:1,dotColor:.white,.circle,.diagonal))
                                                    .frame(height:40)
                                                Button {
                                                    var p = Project(owner: viewState.collaborators.randomElement()!, title: "")
                                                    var t: Task!
                                                    if !taskTitle.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty {
                                                        t = Task(project: p.id, title: taskTitle.trimmingCharacters(in:.whitespacesAndNewlines))
                                                        p = p.alter(by: .adding(.task(t.id)))
                                                    }
                                                    projectsTool = .projects(.display(p))
                                                } label: {
                                                    Text("close")
                                                }
                                            }
                                        }
                                    }
                                }
                            case .projects(.search ): Text("Search Projects")
                            default                 : Text("Select tool"    )
                            }
                        }
                    }
                }.frame(maxWidth:.infinity,alignment:.center)
            }
        }.background(.white)
    }
    private func projectsPanel() -> some View {
        Panel(   title: displayingProject() == nil ? "Projects" : "Project: \(displayingProject()!.title.prefix(30))",
                 selection: $projectsTool,
                 style: .dots     ,
                 height: 200,
                 titleBarColor: .init(white:0.85))
        {
            HStack(spacing:0) {
                VStack(spacing:0) {
                    ScrollView(showsIndicators:false) {
                        ForEach(
                            Array([// selected image            unselected image            button title
                                (("doc.plaintext"           , "doc.plaintext"           ), "inspect project"),
                                (("doc.fill.badge.plus"     , "doc.badge.plus"          ), "add project"    ),
                                (("doc.text.magnifyingglass", "doc.text.magnifyingglass"), "search projects"),
                                  ].enumerated()), id:\.0) { x in // (index, ((selected, unselected), title))
                                      let idx = x.0
                                      let image = (selected:x.1.0.0,unselected:x.1.0.1)
                                      let title = x.1.1
                                      Button {
                                          switch idx {
                                          case 0:  projectsTool = .projects(.display(Project(owner: viewState.collaborators.randomElement()!,title:projectTitle)))
                                          case 1:  projectsTool = .projects(.add(Project(owner: viewState.collaborators.randomElement()!,title:projectTitle)))
                                          case 2:  projectsTool = .projects(.search)
                                          default: projectsTool = .none
                                          }
                                      } label: {
                                          VStack {
                                              Image(systemName:
                                                        (idx == 0 && isProjectDisplayTool(selected:projectsTool))
                                                    || (idx == 1 && isProjectAddTool    (selected:projectsTool))
                                                    || (idx == 2 && isProjectSearchTool (selected:projectsTool))
                                                    ? image.selected : image.unselected)
                                              .foregroundColor(.black)
                                              .background( (idx == 0 && isProjectDisplayTool(selected:projectsTool))
                                                           || (idx == 1 && isProjectAddTool    (selected:projectsTool))
                                                           || (idx == 2 && isProjectSearchTool (selected:projectsTool))
                                                           ? .white :.clear)
                                              .padding(1)
                                              .padding(.top,3)
                                              Text(title)
                                                  .font(.caption2)
                                                  .foregroundColor(.black)
                                                  .background(.white)
                                                  .padding(1)
                                          }
                                          .patterened(with:
                                                        (idx == 0 && isProjectDisplayTool(selected:projectsTool))
                                                      || (idx == 1 && isProjectAddTool    (selected:projectsTool))
                                                      || (idx == 2 && isProjectSearchTool (selected:projectsTool))
                                                      ?  .dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal) : .none)
                                          .frame(width:84,height:54)
                                          .border((idx == 0 && isProjectDisplayTool(selected: projectsTool)) ||
                                                  (idx == 1 && isProjectAddTool(selected: projectsTool)) ||
                                                  (idx == 2 && isProjectSearchTool(selected: projectsTool))
                                                  ?  .gray : .clear,width:1)
                                          .cornerRadius(4)
                                      }
                                  }
                    }.frame(width:98,alignment:.center)
                        .padding(.leading,2)
                        .padding(.top,2)
                        .padding(.trailing,2)
                }.frame(alignment:.leading)
                VStack {
                    projectDrawer()
                }.frame(maxWidth:.infinity,alignment:.center)
            }
        }.background(.white)
    }
    private func todosPanel() -> some View {
        Panel(   title: "Todos",
                 selection: $todosTool,
                 style: .stripes(.vertical),
                 height: 200,
                 secondaryColor: tabs[2].color)
        {
            todosDrawer()
        }.background(.white)
    }
    
    func displayingProject() -> Project? {
        if openDrawer == .projects
            && isProjectDisplayTool(selected:projectsTool),
           let p = projectFromDisplayTool(projectsTool),
           viewState.projects.contains(p)
        {
            return p
        }
        return nil
    }
}

fileprivate func collaboratorFromAddTool(_ tool:Tool) -> Collaborator {
    switch tool {
    case .collaborators(.add(let c)): return c
    default: return Collaborator(name:"")
    }
}
fileprivate func projectFromAddTool(_ tool:Tool, owner:Collaborator) -> Project {
    switch tool {
    case .projects(.add(let p)): return p
    default: return Project(owner:owner,title:"")
    }
}
private extension ContentView {
    @ViewBuilder
    func collaboratorsDrawer() -> some View {
        drawer(tool:collaboratorsTool) { t in
            Group {
                
                switch t {
                case .collaborators(.add):
                    VStack {
                        VStack {
                            HStack {
                                TextField("Enter new name",text:$collaboratorName)
                                    .textFieldStyle(RectangleTextFieldStyle())
                                    .modifier(ClearButton(text:$collaboratorName,color:.black.opacity(0.35)))
                                    .padding(.trailing,12)
                            }
                            HStack {
                                HStack {
                                    Button {
                                        execute(.taskmanager(.add(.collaborator(collaboratorFromAddTool(collaboratorsTool).alter(.name(collaboratorName.trimmingCharacters(in:.whitespacesAndNewlines)))))))
                                        collaboratorsTool = .collaborators(.add(.init(name:"")))
                                        collaboratorName = ""
                                    } label: {
                                        Text("Add Collaborator")
                                            .shadow(color:.white.opacity(0.3),radius:1)
                                    }
                                    .foregroundColor(.black)
                                    .tint(.black)
                                    .disabled(collaboratorName.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty)
                                    .cornerRadius(4)
                                    .border(.gray)
                                    .frame(height:44)
                                }
                            }
                        }
                    }
                    
                case .collaborators(.search): Text("search collaborators").frame(height:44)
                default                     : Text("Select tool"         ).frame(height:44)
                }
            }
        }
    }
    @ViewBuilder
    func projectDrawer() -> some View {
        drawer(tool:projectsTool) { t in
            Group {
                switch t {
                case .projects(.add):
                    VStack {
                        VStack {
                            HStack {
                                TextField("Enter new projectname",text:$projectTitle)
                                    .textFieldStyle(RectangleTextFieldStyle())
                                    .modifier(ClearButton(text:$projectTitle,color:.black.opacity(0.65)))
                                    .padding(.trailing,12)
                            }
                            HStack {
                                HStack {
                                    Button {
                                        execute(.taskmanager(.add(.project(.init(owner:viewState.collaborators.randomElement()!,title:projectTitle.trimmingCharacters(in:.whitespacesAndNewlines))))))
                                        projectsTool = .projects(.add(.init(owner:viewState.collaborators.randomElement()!,title:"")))
                                        projectTitle = ""
                                    } label: {
                                        VStack {
                                            VStack {
                                                Text("Add Project")
                                                    .background(Color.white)
                                                    .font(.body)
                                                
                                            }
                                            .patterened(with:!projectTitle.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty ? .dotted(cellSize:5,dotSize:2,dotColor:.gray,.square,.diagonal) : .none)
                                            .frame(width:100, height:44)
                                            .overlay(
                                                RoundedRectangle(cornerRadius:4)
                                                    .stroke(Color.gray, style:StrokeStyle(lineWidth:2))
                                            ).clipShape(RoundedRectangle(cornerRadius:4))
                                        }
                                    }
                                    .foregroundColor(.black)
                                    .tint(.black)
                                    .disabled(projectTitle.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty)
                                    .frame(height:44)
                                }
                            }
                        }
                    }
                    
                case .projects(.display):
                    if openDrawer == .projects && isProjectDisplayTool(selected:projectsTool) {
                        if let p = projectFromDisplayTool(projectsTool) {
                            if !viewState.projects.contains(p) {
                                Text("select a project from the list above")
                            } else {
                                VStack {
                                    TextField("task title", text:$taskTitle)
                                        .patterened(with:.dotted(cellSize:2,dotSize:1,dotColor:.white,.circle,.diagonal))
                                        .frame(height:40)
                                    Button {
                                        var p = Project(owner: viewState.collaborators.randomElement()!, title: "")
                                        var t: Task!
                                        if !taskTitle.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty {
                                            t = Task(project: p.id, title: taskTitle.trimmingCharacters(in:.whitespacesAndNewlines))
                                            p = p.alter(by: .adding(.task(t.id)))
                                        }
                                        projectsTool = .projects(.display(p))
                                    } label: {
                                        Text("close")
                                    }
                                }
                            }
                        }
                    }
                case .projects(.search ): Text("Search Projects")
                default                 : Text("Select tool"    )
                }
            }
        }
    }
    @ViewBuilder
    func todosDrawer() -> some View {
        drawer(tool:todosTool) { t in
            Group {
                
                switch t {
                case .todos(.add)   : Text("add todo"    )
                case .todos(.search): Text("search todos")
                default             : Text("Select tool" )
                }
            }
            
        }
    }
    func drawer(tool:Tool, _ toolSelection: (Tool) -> some View) -> some View {
        VStack(spacing:0) {
            toolSelection(tool).frame(alignment:.topLeading).padding(.leading,12)
        }
        .padding(10)
        .background(.white)
    }
}
private extension ContentView {
    func listOfCollaborators() -> some View {
        List {
            Section {
                ForEach(viewState.collaborators) { c in
                    HStack {
                        Text(c.name)
                        Spacer()
                    }.contentShape(Rectangle())
                }
            }
        }
        .id(UUID())
        .frame(maxWidth:.infinity)
    }
    func listOfProjects() -> some View {
        List {
            Section {
                ForEach(viewState.projects) { p in
                    HStack {
                        Text(p.title)
                        Spacer()
                    }
                    .contentShape(Rectangle())
                    .onTapGesture { projectsTool = .projects(.display(p)) }
                }
                .onDelete { $0.forEach { execute(.taskmanager(.remove(.project(viewState.projects[$0])))) } }
            }
        }
        .id(UUID())
        .frame(maxWidth:.infinity)
    }
    func listOfTodos() -> some View {
        List {
            Text("Todos").font(.title)
        }.id(UUID())
    }
}

fileprivate func isProjectDisplayTool(selected t:Tool) -> Bool {
    switch t {
    case .projects(.display): return true
    default: return false
    }
}
fileprivate func isProjectAddTool(selected t:Tool) -> Bool {
    switch t {
    case .projects(.add): return true
    default: return false
    }
}

fileprivate func isAProjectTool(selected t:Tool) -> Bool {
    switch t {
    case .projects: return true
    default: return false
    }
}
fileprivate func projectFromDisplayTool(_ tool:Tool) -> Project? {
    switch tool {
    case .projects(.display(let p)): return p
    default: return nil
    }
}
fileprivate func isProjectSearchTool(selected t:Tool) -> Bool {
    switch t {
    case .projects(.search): return true
    default: return false
    }
}
fileprivate func isCollaboratorsAddTool(selected t:Tool) -> Bool {
    switch t {
    case .collaborators(.add): return true
    default: return false
    }
}
fileprivate func isCollaboratorSearchTool(selected t:Tool) -> Bool {
    switch t {
    case .collaborators(.search): return true
    default: return false
    }
}
