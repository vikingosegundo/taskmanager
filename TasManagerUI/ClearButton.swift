//
//  ClearButton.swift
//  TaskManagerUI
//
//  Created by Manuel Meyer on 28.06.22.
//

import SwiftUI
import TaskManagerModels

public struct ClearButton: ViewModifier {
    @Binding var text: String
    private let iconName:String
    private let color: Color
    private let block: () -> ()
    public init(text           t: Binding<String>,
                systemIconName s: String = "multiply.circle.fill",
                color          c: Color = .black,
                _              b: @escaping () -> () = { }
    ) {
        _text    = t
        iconName = s
        color = c
        block    = b
    }
    public func body(content: Content) -> some View {
        GeometryReader { geo in
            HStack(spacing:0) {
                content.frame(width:geo.size.width,alignment:.trailing)
                Image(systemName:iconName)
                    .zIndex(10)
                    .foregroundColor(color)
                    .opacity(text == "" ? 0 : 1)
                    .onTapGesture { text = ""; block() }
                    .padding(.leading,-36)
            }
        }
    }
}

struct OvalTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(.leading , 12)
            .padding(.top     ,  8)
            .padding(.trailing, 36)
            .padding(.bottom  ,  8)
            .background(.white.opacity(0.5))
            .foregroundColor(.black)
            .cornerRadius(20)
    }
}
struct RectangleTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(.leading ,  8)
            .padding(.top     ,  8)
            .padding(.trailing, 36)
            .padding(.bottom  ,  8)
            .background(.white.opacity(0.5))
            .foregroundColor(.black)
    }
}
