//
//  PaternView_Previews.swift
//  TaskManagerUI
//
//  Created by Manuel Meyer on 17.07.22.
//

import SwiftUI
struct PaternView_Previews:PreviewProvider {
    @State static var celsius         : Double = 0
    @State static var text            : String = ""
    @State static var toggled         : Bool   = true
    @State static var untoggled       : Bool   = false
    @State static var selectedSubpanel: Int    = 0
    static var previews:some View {
        GeometryReader { geo in
            ScrollView {
                Panel(title:"Projects 0",selection:$selectedSubpanel,style:.dots,height:170,titleBarColor:.init(white:0.85)) {
                    HStack(spacing:0) {
                        VStack(spacing:0) {
                            VStack(spacing:0) {
                                ScrollView {
                                    Button {
                                    } label: {
                                        VStack {
                                            Image(systemName:"doc.plaintext")
                                                .foregroundColor(.black)
                                                .background(.clear)
                                                .padding(1)
                                                .padding(.top,3)
                                            Text("inspect project")
                                                .font(.caption2)
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                        }
                                        .patterened(with:.none)
                                        .frame(width:84,height:54)
                                        .border(.clear,width:1)
                                        .cornerRadius(4)
                                    }
                                    Button {
                                    } label: {
                                        VStack {
                                            Image(systemName:"doc.fill.badge.plus")
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                                .padding(.top,3)
                                            Text("add project")
                                                .font(.caption2)
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                        }
                                        .patterened(with:.dotted(cellSize:5,dotSize:1,dotColor:.gray,.square,.diagonal))
                                        .frame(width:74,height:54)
                                        .overlay(
                                            RoundedRectangle(cornerRadius:4)
                                                .stroke(Color.gray, style:StrokeStyle(lineWidth:2))
                                        ).clipShape(RoundedRectangle(cornerRadius:4))
                                    }
                                    Button {
                                    } label: {
                                        VStack {
                                            Image(systemName:"doc.text.magnifyingglass")
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                                .padding(.top,3)
                                            Text("search projects")
                                                .font(.caption2)
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                        }
                                        .patterened(with:.none)
                                        .frame(width:84,height:54)
                                        .overlay(
                                            RoundedRectangle(cornerRadius:4)
                                                .stroke(Color.clear, style:StrokeStyle(lineWidth:2))
                                        ).clipShape(RoundedRectangle(cornerRadius:4))
                                    
                                    }
                                }
                                
                            }.frame(width:94,alignment:.leading)
                                .padding(.leading,2)
                                .padding(.top,2)
                                .padding(.trailing,2)
                        }.frame(alignment:.leading)
                        VStack {
                            
                        }.frame(maxWidth:.infinity,alignment:.center)
                        
                    }
                }
                Panel(title:"Projects 1",selection:$selectedSubpanel,style:.stripes(.horizontal),height:150,dotColor:.red,secondaryColor:.yellow,titleBarColor:.init(white:0.2)) {
                    HStack(spacing:0) {
                        VStack(spacing:0) {
                            ScrollView(showsIndicators:false) {
                                VStack {
                                    Button {
                                    } label: {
                                        VStack {
                                            Image(systemName:"doc.plaintext")
                                                .foregroundColor(.orange)
                                                .background(.white)
                                                .padding(1)
                                                .padding(.top,3)
                                            Text("inspect project")
                                                .font(.caption2)
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                        }
                                        .patterened(with:.none)
                                        .frame(width:84,height:54)
                                        .overlay(
                                            RoundedRectangle(cornerRadius:4)
                                                .stroke(Color.clear, style:StrokeStyle(lineWidth:2))
                                        ).clipShape(RoundedRectangle(cornerRadius:4))
                                    
                                    }
                                    Button {
                                    } label: {
                                        VStack {
                                            Image(systemName:"doc.fill.badge.plus")
                                                .foregroundColor(.orange)
                                                .background(.white)
                                                .padding(1)
                                                .padding(.top,3)
                                            Text("add project")
                                                .font(.caption2)
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                        }
                                        .patterened(with:.dotted(cellSize:5,dotSize:2,dotColor:.orange,.square,.diagonal))
                                        .frame(width:74,height:54)
                                        .overlay(
                                            RoundedRectangle(cornerRadius:4)
                                                .stroke(Color.orange, style:StrokeStyle(lineWidth:2))
                                        ).clipShape(RoundedRectangle(cornerRadius:4))
                                    
                                    }
                                    Button {
                                    } label: {
                                        VStack {
                                            Image(systemName:"doc.text.magnifyingglass")
                                                .foregroundColor(.orange)
                                                .background(.white)
                                                .padding(1)
                                                .padding(.top,3)
                                            Text("search projects")
                                                .font(.caption2)
                                                .foregroundColor(.black)
                                                .background(.white)
                                                .padding(1)
                                        }
                                        .patterened(with:.none)
                                        .frame(width:84,height:54)
                                        .overlay(
                                            RoundedRectangle(cornerRadius:4)
                                                .stroke(Color.clear, style:StrokeStyle(lineWidth:2))
                                        ).clipShape(RoundedRectangle(cornerRadius:4))
                                    
                                    }
                                }
                                .padding(.leading,2)
                                .padding(.top,2)
                                .padding(.trailing,2)
                            }
                        }
                        VStack {
                            ScrollView {
                                VStack {
                                    Slider(value:$celsius,in:-100...100).tint(.orange)
                                    Slider(value:$celsius,in:-100...100).tint(.orange)
                                    Toggle("activate", isOn: $toggled)
                                        .toggleStyle(CheckToggleStyle(tint:.orange))
                                    Slider(value:$celsius,in:-100...100).tint(.orange)
                                    Spacer()
                                }
                                .frame(width:.infinity)
                                .padding(.top,8)
                                .padding(.trailing,8)
                            }
                        }
                        .frame(alignment:.center)
                    }
                }
                Panel(title:"Projects 2",selection:$selectedSubpanel,style:.stripes(.vertical),height:180,dotColor:.init(hue:0.66,saturation:0.6,brightness:0.7),secondaryColor:.init(hue:0.5,saturation:0.5,brightness:0.6)) {
                    VStack {
                        HStack {
                            Button {
                            } label: {
                                VStack {
                                    Image(systemName:"doc.plaintext")
                                        .foregroundColor(.black)
                                        .background(.white)
                                        .padding(1)
                                        .padding(.top,3)
                                    Text("inspect project")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .background(.white)
                                        .padding(1)
                                }
                                .patterened(with:.none)
                                .frame(width:84,height:54)
                                .overlay(
                                    RoundedRectangle(cornerRadius:4)
                                        .stroke(Color.clear, style:StrokeStyle(lineWidth:2))
                                ).clipShape(RoundedRectangle(cornerRadius:4))
                            
                            }
                            Button {
                            } label: {
                                VStack {
                                    Image(systemName:"doc.fill.badge.plus")
                                        .foregroundColor(.white)
                                        .background(.clear)
                                        .padding(1)
                                        .padding(.top,3)
                                    Text("add project")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .background()
                                        .padding(1)
                                }
                                .patterened(with:.dotted(cellSize:5,dotSize:2,dotColor:.init(hue:0.66,saturation:0.6,brightness:0.7),.square,.diagonal))
                                .background(Color(hue:0.5,saturation:0.5,brightness:0.6))
                                .frame(width:74,height:54)
                                .overlay(
                                    RoundedRectangle(cornerRadius:4)
                                        .stroke(Color(hue:0.66,saturation:0.6,brightness:0.7), style:StrokeStyle(lineWidth:2))
                                ).clipShape(RoundedRectangle(cornerRadius:4))
                            
                            }
                            Button {
                            } label: {
                                VStack {
                                    Image(systemName:"doc.text.magnifyingglass")
                                        .foregroundColor(.black)
                                        .background(.white)
                                        .padding(1)
                                        .padding(.top,3)
                                    Text("search projects")
                                        .font(.caption2)
                                        .foregroundColor(.black)
                                        .background(.white)
                                        .padding(1)
                                }
                                .patterened(with:.none)
                                .frame(width:84,height:54)
                                .overlay(
                                    RoundedRectangle(cornerRadius:4)
                                        .stroke(Color.clear, style:StrokeStyle(lineWidth:2))
                                ).clipShape(RoundedRectangle(cornerRadius:4))
                            
                            }
                        }
                        .padding(.top,8)
                        Slider(value:$celsius,in:-100...100).padding(12).padding(.bottom,0).tint(.init(hue:0.5,saturation:0.5,brightness:0.6))
                        HStack {
                            TextField("Text", text: $text)
                                .padding(4)
                                .padding(.bottom,0)
                                .border(Color(hue:0.5,saturation:0.5,brightness:0.6),width:1)
                            Button {} label: {
                                Text("OK")
                            }.patterened(with:.dotted(cellSize:5,dotSize:2,dotColor:.init(hue:0.66,saturation:0.6,brightness:0.7),.square,.diagonal))
                                .background(Color(hue:0.5,saturation:0.5,brightness:0.6))
                                .foregroundColor(.white)
                                .frame(width: 44)
                        }.padding(8)
                    }.frame(maxWidth:.infinity,alignment:.center)
                    
                }
            }.padding(.leading,8).padding(.trailing,8)
        }
        ScrollView {
            GeometryReader { geo in
                VStack {
                    Spacer()
                    VStack {
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:15,dotSize:9,dotColor:.black,.circle,.diagonal),contentBackgroundColor:.init(white:0.9)))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .patterened(with:.dotted(cellSize:5,dotSize:3,dotColor:.black,.circle,.parallel),contentBackgroundColor:.init(white:0.9))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:5,dotSize:2,dotColor:.black,.circle,.diagonal),contentBackgroundColor:.init(white:0.9)))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                    }
                    VStack {
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:7,dotSize:4,dotColor:.black,.cross,.diagonal),contentBackgroundColor:.init(white:0.9)))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:7,dotSize:4,dotColor:.black,.cross,.parallel),contentBackgroundColor:.init(white:0.9)))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:10,dotSize:2,dotColor:.black,.circle,.diagonal),contentBackgroundColor:.init(white:0.9)))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .patterened(with:.stripped(with:[.init(color:.black),.init(color:.black),.init(color:.black),.init(color:.white), .init(color:.white),.init(color:.white)], .vertical),contentBackgroundColor:.init(white:0.9))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .patterened(with:.stripped(with:[.init(color:.black),.init(color:.white)],.horizontal),contentBackgroundColor:.init(white:0.9))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .patterened(with:.stripped(with:[.init(color:.black),.init(color:.white),.init(color:.white),.init(color:.white)],.horizontal),contentBackgroundColor:.init(white:0.9))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                    }
                    if #available(iOS 16.0, *) {
                        Grid {
                            GridRow {
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.black,.circle,.diagonal))
                                        .frame(width:48,height:48)
                                        .background(.white)
                                        .clipShape(Circle())
                                }
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.black,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(.gray)
                                        .clipShape(Circle())
                                }
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.white,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(.gray)
                                        .clipShape(Circle())
                                }
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .checkered(.gray,.white,cellWidth:5)
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(Circle())
                                }
                            }
                            GridRow {
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.black,.circle,.diagonal))
                                        .frame(width:48,height:48)
                                        .background(.white)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.black,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(Color(white:0.7))
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.white,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(Color(white:0.4))
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                                ZStack {
                                    Color.black
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .checkered(.white,cellWidth:5)
                                        .frame(width:45,height:45)
                                        .background(Color(white:0.4))
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                            }
                        }
                    }
                }
            }
        }.padding(.leading,8).padding(.trailing,8)
        ScrollView {
            GeometryReader { geo in
                VStack {
                    VStack {
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:15,dotSize:9,dotColor:.orange,.circle,.diagonal),contentBackgroundColor:.orange))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.gray)
                        Text("Hello")
                            .modifier(Pattern(style:.dotted(cellSize:5,dotSize:2,dotColor:.black,.circle,.diagonal),contentBackgroundColor:.orange))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.orange)
                        
                        Text("Hello")
                            .patterened(with:.dotted(cellSize:10,dotSize:2,dotColor:.orange,.circle,.diagonal))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.yellow)
                    }
                    VStack {
                        Text("Hello")
                            .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.yellow,.square,.diagonal),contentBackgroundColor:.green.opacity(0.5))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.green)
                        Text("Hello")
                            .patterened(with:.dotted(cellSize:4,dotSize:3,dotColor:.yellow,.square,.parallel),contentBackgroundColor:.green.opacity(0.5))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.green)
                        Text("Hello")
                            .checkered(.yellow,.green,cellWidth:5)
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.green)
                        Text("Hello")
                            .checkered(.yellow,.green,.yellow.opacity(0.75),cellWidth:2)
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.green)
                        Text("Hello")
                            .checkered(.blue,.yellow,.gray,cellWidth:2)
                            .frame(width:geo.size.width,height:30,alignment:.center)
                        Text("Hello")
                            .patterened(with:.dotted(cellSize:3,dotSize:2,dotColor:.yellow,.square,.parallel),
                                        contentBackgroundColor:.init(hue:0.6,saturation:0.25,brightness:0.75))
                            .frame(width:geo.size.width,height:30,alignment:.center).background(.blue)
                    }
                    if #available(iOS 16.0, *) {
                        Grid {
                            GridRow {
                                ZStack {
                                    Color.red
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.orange,.circle,.diagonal))
                                        .frame(width:48,height:48)
                                        .background(.white)
                                        .clipShape(Circle())
                                }
                                ZStack {
                                    Color.red
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:6,dotSize:4,dotColor:.orange,.cross,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(Circle())
                                }
                                ZStack {
                                    Color.orange
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.orange,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(Circle())
                                }
                                ZStack {
                                    Color.orange
                                        .frame(width:50,height:50)
                                        .clipShape(Circle())
                                    EmptyView()
                                        .checkered(.orange,.yellow,cellWidth:5)
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(Circle())
                                }
                            }
                            GridRow {
                                ZStack {
                                    Color.red
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.orange,.circle,.diagonal))
                                        .frame(width:48,height:48)
                                        .background(.white)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                                ZStack {
                                    Color.red
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.orange,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                                ZStack {
                                    Color.orange
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .patterened(with:.dotted(cellSize:10,dotSize:5,dotColor:.white,.circle,.diagonal))
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                                ZStack {
                                    Color.orange
                                        .frame(width:50,height:50)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                    EmptyView()
                                        .checkered(.orange,.yellow,cellWidth:5)
                                        .frame(width:45,height:45)
                                        .background(.yellow)
                                        .clipShape(RoundedRectangle(cornerSize:.init(width:12,height:12)))
                                }
                            }
                        }
                    }
                }
            }
        }.padding(.leading,8).padding(.trailing,8)
    }
}
