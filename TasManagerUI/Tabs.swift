//
//  MainPane.swift
//  TaskManagerUI
//
//  Created by Manuel Meyer on 08.07.22.
//

import SwiftUI

struct Tab {
    let icon: Image?
    let title: String
    let color:Color
}
struct Tabs: View {
    var tabs: [Tab]
    var geoWidth: CGFloat
    @Binding var selectedTab: Int
    var body: some View {
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        ForEach(0 ..< tabs.count, id:\.self) { row in
                            Button(action: {   selectedTab = row  },
                                   label: {
                                VStack(spacing: 0) {
                                    HStack {
                                        tabs[row].icon
                                            .foregroundColor(.white)
                                            .padding(.init(top: 0, leading: 15, bottom: 0, trailing: 0))
                                        Text(tabs[row].title)
                                            .font(.system(size: 18, weight: .semibold))
                                            .foregroundColor(.white)
                                            .padding(.init(top: 10, leading: 3, bottom: 10, trailing: 15))
                                    }
                                    .frame(
                                        width: (geoWidth / CGFloat(tabs.count)),
                                        height: 52)
                                    Rectangle().fill(selectedTab == row ? .black.opacity(0.1) : .clear)
                                        .frame(height: 3)
                                }.fixedSize()
                            })
                            .accentColor(.white)
                            .buttonStyle(PlainButtonStyle())
                            .background(tabs[row].color)
                        }
                }
        }
    }
}
    
struct Tabs_Previews: PreviewProvider {
    static var previews: some View {
        Tabs(tabs: [.init(icon:Image(systemName:"star.fill"), title: "Tab 1", color: .orange),
                    .init(icon:Image(systemName:"star.fill"), title: "Tab 2", color: .green ),
                    .init(icon:Image(systemName:"star.fill"), title: "Tab 3", color: .teal  )],
             geoWidth: 375,
             selectedTab: .constant(0))
    }
}
