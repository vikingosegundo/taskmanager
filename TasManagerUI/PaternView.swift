//
//  PaternView.swift
//  TaskManagerUI
//
//  Created by Manuel Meyer on 12.07.22.
//

import SwiftUI

public struct PaternView:View {
    public enum Style {
        case none
        case dotted(cellSize:Double,dotSize:Double,dotColor:Color,Dotted.Shape,Dotted.Orientation)
        case stripped(with:[Stripped.Stripe], Stripped.Orientation)
        public enum Dotted {
            public enum Shape       { case circle  , square  , cross }
            public enum Orientation { case parallel, diagonal        }
        }
        public enum Stripped {
            public struct Stripe    { let color:Color           }
            public enum Orientation { case horizontal, vertical }
        }
    }
    let style:Style
    public var body:some View {
        switch style {
        case     .none: EmptyView()
        case let .dotted  (cellSize:gs,dotSize:ds,dotColor:dc,sh,o): dottedView(shape:sh,cellSize:gs,dotWidth:ds,dotColor:dc,orientation:o)
        case let .stripped(with:stripes,oriented)                  : stripedView(stripes:stripes,oriented:oriented)
        }
    }
    private func stripedView(stripes:[Style.Stripped.Stripe],oriented:Style.Stripped.Orientation) -> some View {
        GeometryReader { geo in
            switch oriented {
            case .vertical:
                let stripes = Array(repeating:stripes, count:(Int(geo.size.width) / stripes.count) + 1).flatMap{$0}
                ForEach(Array(stripes.enumerated()),id:\.0) { (i,s) in
                    Path { path in
                        path.move(to:.init(x:i-1, y:0))
                        path.addLine(to:.init(x:Double(i-1),y:geo.size.width))
                    }.stroke(s.color)
                }
            case .horizontal:
                let stripes = Array(repeating:stripes, count:(Int(geo.size.width) / stripes.count) + 1).flatMap{$0}
                ForEach(Array(stripes.enumerated()),id:\.0) { (i,s) in
                    Path { path in
                        path.move(to:.init(x:0,y:Double(i-1)))
                        path.addLine(to:.init(x:Double(i-1) * geo.size.width ,y:0))
                    }.stroke(s.color)
                }
            }
        }
    }
    private func dottedView(shape:Style.Dotted.Shape,cellSize:Double,dotWidth:Double,dotColor:Color,orientation:Style.Dotted.Orientation) -> some View {
        GeometryReader { geo in
            ForEach(-1...Int(geo.size.width / cellSize) + 1,id:\.self) { v in
                ForEach(-1...Int(geo.size.width / cellSize) + 1,id:\.self) { h in
                    switch (shape,orientation) {
                    case (.cross,.diagonal):
                        Path { path in
                            
                            path.move   (to:.init(x:Double(h) * cellSize + cellSize / 2 - dotWidth / 2,
                                                  y:Double(v) * cellSize - dotWidth / 2 - dotWidth / 2))
                            path.addLine(to:.init(x:Double(h) * cellSize + cellSize / 2 + dotWidth / 2,
                                                  y:Double(v) * cellSize - dotWidth / 2 + dotWidth / 2))
                            path.move   (to:.init(x:Double(h) * cellSize + cellSize / 2 - dotWidth / 2,
                                                  y:Double(v) * cellSize - dotWidth / 2 + dotWidth / 2))
                            path.addLine(to:.init(x:Double(h) * cellSize + cellSize / 2 + dotWidth / 2,
                                                  y:Double(v) * cellSize - dotWidth / 2 - dotWidth / 2))
                        }.stroke(dotColor)
                    case (.cross,.parallel):
                        Path { path in
                            
                            path.move   (to:.init(x:Double(h) * cellSize + cellSize,
                                                  y:Double(v) * cellSize - dotWidth / 2 - dotWidth / 2))
                            path.addLine(to:.init(x:Double(h) * cellSize + cellSize,
                                                  y:Double(v) * cellSize - dotWidth / 2 + dotWidth / 2))
                            path.move   (to:.init(x:Double(h) * cellSize - dotWidth / 2,
                                                  y:Double(v) * cellSize - dotWidth / 2))
                            path.addLine(to:.init(x:Double(h) * cellSize + dotWidth / 2,
                                                  y:Double(v) * cellSize - dotWidth / 2))
                            
                        }.stroke(dotColor)
                        
                    case (.circle,.parallel):
                        Path { path in
                            path
                                .addRoundedRect(
                                    in:.init(
                                        x:Double(h) * cellSize - dotWidth / 2,
                                        y:Double(v) * cellSize - dotWidth / 2,
                                        width:dotWidth,
                                        height:dotWidth
                                    ),
                                    cornerSize:.init(width:dotWidth,height:dotWidth)
                                )
                        }
                    case (.circle,.diagonal):
                        Path { path in
                            path
                                .addRoundedRect(
                                    in:.init(
                                        x:Double(h) * cellSize + cellSize / 2 - dotWidth / 2,
                                        y:Double(v) * cellSize - dotWidth / 2,
                                        width:dotWidth,
                                        height:dotWidth
                                    ),
                                    cornerSize:.init(width:dotWidth,height:dotWidth)
                                )
                            path.addRoundedRect(
                                in:.init(
                                    x:Double(h) * cellSize - ( dotWidth / 2),
                                    y:Double(v) * cellSize - (  cellSize / 2 + ( dotWidth / 2)),
                                    width:dotWidth,
                                    height:dotWidth
                                ),cornerSize:.init(width:dotWidth,height:dotWidth))
                        }.fill(dotColor)
                    case (.square,.parallel):
                        Path { path in
                            path.addRect(.init(
                                x:Double(h) * cellSize - ( dotWidth / 2),
                                y:Double(v) * cellSize - (  cellSize / 2 + ( dotWidth / 2)),
                                width:dotWidth,
                                height:dotWidth
                            ))
                        }.fill(dotColor)
                    case (.square,.diagonal):
                        Path { path in
                            path
                                .addRect(
                                    .init(
                                        x:Double(h) * cellSize + cellSize / 2 - dotWidth / 2,
                                        y:Double(v) * cellSize - dotWidth / 2,
                                        width:dotWidth,
                                        height:dotWidth
                                    ))
                            
                            path.addRect(
                                .init(
                                    x:Double(h) * cellSize -  (dotWidth / 2),
                                    y:Double(v) * cellSize - ((dotWidth / 2) + cellSize / 2),
                                    width:dotWidth,
                                    height:dotWidth
                                ))
                        }.fill(dotColor)
                    }
                }
            }
        }
    }
}
public struct Pattern:ViewModifier {
    private let style:PaternView.Style
    private let backgroundColor:Color
    public init(style _s:PaternView.Style,contentBackgroundColor _b:Color = .white) { style = _s; backgroundColor = _b }
    public func body(content:Content) -> some View {
        ZStack {
            PaternView(style:style)
                .clipShape(Rectangle())
            content
                .background(backgroundColor)
        }
    }
}
public extension View {
    func patterened(with style:PaternView.Style,contentBackgroundColor cb:Color = .clear) -> some View {
        modifier(Pattern(style:style,contentBackgroundColor:cb))
    }
}
public extension View {
    func checkered(_ firstColor:Color,_ secondColor:Color = .clear,_ contentBackground:Color = .clear,cellWidth:Double) -> some View {
        modifier(Pattern(style:.dotted(cellSize:cellWidth * 2,dotSize:cellWidth,dotColor:firstColor,.square,.diagonal),contentBackgroundColor:contentBackground)).background(secondColor)
    }
}
public struct Panel<Content:View, Selection>:View {
    public enum Style:Equatable {
        case stripes(StripesOrientation)
        case dots
        case checkered
        public enum StripesOrientation:Equatable {
            case horizontal
            case vertical
        }
    }
    private let title         : String
    private let style         : Style
    private let height        : Double
    private let dotColor      : Color
    private let secondaryColor: Color
    private let titleBarColor : Color
    private var content       : () -> Content
    private var selection     :Binding<Selection>
    public init(
        title           t: String,
        selection      sl: Binding<Selection>,
        style           s: Style = .stripes(.horizontal),
        height          h: Double = 140,
        dotColor       dc: Color = .black,
        secondaryColor sc: Color = .gray,
        titleBarColor  tc: Color = .black,
        content         c: @escaping () -> Content
    ) {
        title          =  t
        selection      = sl
        style          =  s
        height         =  h
        dotColor       = dc
        secondaryColor = sc
        titleBarColor  = tc
        content        =  c
    }
    public var body:some View {
        VStack(spacing:0) {
            VStack(spacing:0) {
                VStack(spacing:0)  {
                    VStack(spacing:0)  {
                        Text(title).foregroundColor(titleBarColor)
                    }.patterened(
                        with:
                            style == .stripes(.horizontal)
                        ? .stripped(with:[.init(color:dotColor),.init(color:.clear)], .horizontal)
                        : style == .stripes(.vertical)
                        ? .stripped(with:[.init(color:dotColor),.init(color:.clear),.init(color:.clear),.init(color:.clear)], .vertical)
                        : style == .dots
                        ? .dotted(cellSize:5,dotSize:2,dotColor:dotColor,.circle,.diagonal)
                        : .dotted(cellSize:4,dotSize:1,dotColor:dotColor,.square,.diagonal),
                        contentBackgroundColor:secondaryColor)
                    .frame(height:28, alignment:.center )
                    .background(secondaryColor)
                    VStack(spacing:0) {
                        VStack {
                            VStack(spacing:0,content:content)
                                .frame(maxWidth:.infinity)
                        }
                    }
                    .frame(height:height)
                    .border(secondaryColor,width:2)
                }
                .frame(alignment:.center)
                .clipShape(Rectangle())
            }
        }
    }
}
struct CheckToggleStyle: ToggleStyle {
    let tint:Color?
    func makeBody(configuration: Configuration) -> some View {
        Button {
            configuration.isOn.toggle()
        } label: {
            Label {
                configuration.label
            } icon: {
                Image(systemName: configuration.isOn ? "checkmark.square.fill" : "square")
                    .foregroundColor(configuration.isOn ? tint != nil ? tint : .accentColor : .secondary)
                    .accessibility(label: Text(configuration.isOn ? "Checked" : "Unchecked"))
                    .imageScale(.large)
            }
        }
        .buttonStyle(PlainButtonStyle())
    }
}
