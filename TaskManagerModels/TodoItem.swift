//
//  TodoItem.swift
//  TaskManagerModels
//
//  Created by Manuel Meyer on 02.07.22.
//

import Foundation.NSUUID
import Tools

public struct TodoItem:TypedIdentifiable, Codable {
    public typealias IDType = UUID
    public let id       : ID
    public let text     : String
    public let completed: Bool
    public let dueDate  : Date?
    public var previous : Self? { meta.previous     }
    public var created  : Date  { meta.creationDate }
    
    public enum Change {
        case setting   (Setting)
        case modifying (Modify  )
        case forking   ([Change])
        case execute   ((TodoItem) -> ())
        public enum Setting {
            case text     (to:String)
            case completed(to:Bool  )
            case dueDate  (to:Date? )
        }
        public enum Modify {
            case text(by:Modification.Text,String)
            case dueDate(by:Modification.Date, Modification.Date.Unit)
            public enum Modification {
                public enum Text { case appending, prepending }
                public enum Date { case postponingIt(by:Int) ; public enum Unit { case day, week } }
            }
        }
    }
    public init(text t:String) { self.init(.init(value:UUID()),t,false,nil,.init(Date())) }
    public func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter($1,metaID:Identifier<UUID,Self.Meta>(value:UUID())) } }
    public func alter(by changes:[Change]  ) -> Self { changes.reduce(self) { $0.alter($1,metaID:Identifier<UUID,Self.Meta>(value:UUID())) } }
    fileprivate let meta:Meta
}
extension TodoItem {
    private init (_ i:ID,_ t:String,_ c:Bool,_ d:Date?,_ m:Meta) { id=i; text=t; completed=c; dueDate=d; meta=m }
    private func alter(_   change:Change, metaID:Meta.ID) -> Self { item(with:change, metaID:metaID) }
    private func item(with change:Change, metaID:Meta.ID) -> Self {
        let altered = Date()
        switch change {
        case let .setting  (c):          return    setting(  by:c,metaID,altered)
        case let .modifying(m):          return  modifying(  by:m,metaID,altered)
        case let .forking  (c):          return    forking(with:c)
        case let .execute(exe):exe(self);return self
        }
    }
    private func setting(by c:Change.Setting,_ metaID:Meta.ID,_ altered:Date) -> Self {
        switch c {
        case let      .text(to:text     ): return .init(id,text,completed,dueDate,.init(metaID,meta.creationDate,altered,self))
        case let .completed(to:completed): return .init(id,text,completed,dueDate,.init(metaID,meta.creationDate,altered,self))
        case let   .dueDate(to:dueDate  ): return .init(id,text,completed,dueDate,.init(metaID,meta.creationDate,altered,self))
        }
    }
    private func modifying(by c:Change.Modify,_ metaID:Meta.ID,_ altered:Date) -> Self {
        let cal = Calendar.autoupdatingCurrent
        switch c {
        case let .text   (by: .appending        ,t    ): return .init(id,text+"\(t)",completed,dueDate,.init(metaID,meta.creationDate,altered,self))
        case let .text   (by:.prepending        ,t    ): return .init(id,"\(t)"+text,completed,dueDate,.init(metaID,meta.creationDate,altered,self))
        case let .dueDate(by:.postponingIt(by:i),.day ): return .init(id,text,completed,cal.date(byAdding:.day,value:i,  to:dueDate ?? Date()),.init(metaID,meta.creationDate,altered,self))
        case let .dueDate(by:.postponingIt(by:i),.week): return .init(id,text,completed,cal.date(byAdding:.day,value:i*7,to:dueDate ?? Date()),.init(metaID,meta.creationDate,altered,self))
        }
    }
    private func forking(with c:[Change]) -> Self { // override id and meta.id to cretae new object with same history
        .init(Identifier(value:UUID()),text,completed,dueDate,.init(Identifier<UUID,Self.Meta>(value:UUID()),meta.creationDate,meta.alterDate,self)).alter(by:c)
    }
}
extension TodoItem {
    fileprivate
    struct Meta: TypedIdentifiable, Codable {
        typealias IDType = UUID
        let id          : Meta.ID
        let creationDate: Date
        let alterDate   : Date?
        var previous    : TodoItem? { _previous.first }
        private
        let _previous   : [TodoItem]
        init(              _ c:Date                       ) { id=Identifier(value:UUID());creationDate=c;alterDate=nil;_previous=[ ] }
        init(_ _id:Meta.ID,_ c:Date,_ a:Date?,_ p:TodoItem) { id=_id                     ;creationDate=c;alterDate=a  ;_previous=[p] }
    }
}
extension TodoItem: CustomStringConvertible {
    public var description:String {
        "\(text)\t\(completed ? "✓" : "𐄂") \(dueDate != nil ? "\tdue:"+dueDate!.description : "")\tcreated:\(meta.creationDate)\n\(meta.previous != nil ? meta.previous!.description : "")"
    }
}
