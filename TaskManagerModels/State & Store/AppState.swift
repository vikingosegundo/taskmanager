//
//  AppState.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//
import Tools

public func change(_ state:AppState,_ change: AppState.Change...) -> AppState { state.alter(change) }
public func change(_ state:AppState,_ change:[AppState.Change]  ) -> AppState { state.alter(change) }

public struct AppState {
    public enum Change {
        case by(_By)
        public enum _By {
            case adding  (_Add    )
            case updating(_Update )
            case removing(_Remove )
        }
        public enum _Add    { case project(Project), task(Task), subtask(SubTask), collaborator(Collaborator) }
        public enum _Update { case project(Project)                               }
        public enum _Remove { case project(Project), task(Task)                   }
    }
    public init() { self.init([],[],[],[]) }
    public func alter(_ changes:Change...) -> AppState { changes.reduce(self) { $0.alter($1) } }
    public func alter(_ changes:[Change] ) -> AppState { changes.reduce(self) { $0.alter($1) } }

    public let projects     : [Project]
    public let tasks        : [Task   ]
    public let subtasks     : [SubTask]
    public let collaborators: [Collaborator]
}
extension AppState               : Codable   {}
extension AppState.Change        : Equatable {}
extension AppState.Change._By    : Equatable {}
extension AppState.Change._Add   : Equatable {}
extension AppState.Change._Remove: Equatable {}
extension AppState.Change._Update: Equatable {}

//MARK: - private
private extension AppState {
    private init(_ _projects:[Project],_ _tasks:[Task],_ _subtasks:[SubTask], _ _collaborators:[Collaborator]) { projects=_projects; tasks=_tasks; subtasks=_subtasks; collaborators=_collaborators }
    private func alter(_ change:Change) -> AppState {
        switch change {
        case let .by(.adding  (msg)): return    add(msg)
        case let .by(.updating(msg)): return update(msg)
        case let .by(.removing(msg)): return remove(msg)
        }
    }
    private func add   (_ change: Change._Add   ) -> AppState {
        switch change {
        case let .project     (p): return .init(projects.filter({$0.id != p.id})+[p], tasks,                             subtasks                            , collaborators)
        case let .task        (t): return .init(projects                            , tasks.filter({$0.id != t.id})+[t], subtasks                            , collaborators)
        case let .subtask     (s): return .init(projects                            , tasks                            , subtasks.filter({$0.id != s.id})+[s], collaborators)
        case let .collaborator(c): return .init(projects, tasks, subtasks, collaborators+[c])
        }
    }
    private func update(_ change: Change._Update) -> AppState {
        switch change {
        case let .project(p): return .init(projects.filter({$0.id != p.id})+[p], tasks, subtasks, collaborators)
        }
    }
    private func remove(_ change: Change._Remove) -> AppState {
        switch change {
        case let .project(p): return .init(projects.filter({$0.id != p.id}), tasks                        , subtasks, collaborators)
        case let .task   (t): return .init(projects                        , tasks.filter({$0.id != t.id}), subtasks, collaborators)
        }
    }
}
