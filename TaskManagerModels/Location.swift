//
//  Location.swift
//  TaskManagerModels
//
//  Created by Manuel Meyer on 18.06.22.
//

import Foundation.NSUUID
import Tools

public func alter(_ a:Location.Address,by c:Location.Address.Change) -> Location.Address { a.alter(by:c) }
public enum Location {
    case address   (Address   )
    case coordinate(Coordinate)
    case textual   (String    )
    case unknown
}
extension Location {
    public struct Coordinate {
        public let latitude :Double
        public let longitude:Double
    }
    public struct Address:TypedIdentifiable {
        public typealias IDType = UUID
        public enum Change {
            case changing(_Changing)
            public enum _Changing {
                case street(to:String)
                case city  (to:String)
            }
        }
        public init(street s:String, city c:String) { self.init(Identifier(value:UUID()), s, c)}
        public func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
        public let id    :ID
        public let street:String
        public let city  :String
    }
}
extension Location.Coordinate:Codable,Hashable,Equatable {}
extension Location.Address   :Codable,Hashable,Equatable {}
extension Location           :Codable,Hashable,Equatable {}

private extension Location.Address {
    init(_ _id:ID,_ s: String,_ c: String) { id = _id; street = s; city = c }
    func alter(by c:Change) -> Self {
        switch c {
        case .changing(.street(let street)): return .init(id, street, city)
        case .changing(.city  (let city  )): return .init(id, street, city)
        }
    }
}
