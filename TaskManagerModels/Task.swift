//
//  Task.swift
//  TaskManager
//
//  Created by Manuel Meyer on 15.06.22.
//

import Foundation.NSUUID
import Tools

public func alter(_ t:Task, by change:Task.Change) -> Task { t.alter(by:change) }

public struct Task: TypedIdentifiable {
    public typealias IDType = UUID
    public enum Change {
        public enum _Changing {
            case title   (to:String    )
            case text    (to:String    )
            case project (to:Project.ID)
            case location(to:Location  )
            case state   (to:State     )
        }
        case changing(_Changing)
    }
    public enum State {
        case unfinished(_Unfinished)
        case finished
        public enum _Unfinished {
            case unstarted
            case inprogress
        }
    }
    public let id      : ID
    public let project : Project.ID
    public let title   : String
    public let text    : String
    public let location: Location
    public let state   : State
}
public extension Task {
    init(project:Project.ID, title:String) { self.init(Identifier(value:UUID()), project, title, "", .unknown, .unfinished(.unstarted)) }
    func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
}
extension Task: CustomStringConvertible { public var description: String { "Task «\(id)» in Project «\(project)»: \(title)" } }
extension Task.State._Unfinished: Codable, Hashable {}
extension Task.State            : Codable, Hashable {}
extension Task                  : Codable, Hashable {}

private extension Task {
    init(_ _id:ID,_ _project:Project.ID,_ _title:String,_ _text:String,_ _location:Location,_ _state:State) { id = _id; project = _project; title = _title; text = _text; location = _location; state = _state }
    func alter(by c:Change) -> Self {
        switch c {
        case let .changing(.project (to:project )): return .init(id, project, title, text, location, state)
        case let .changing(.title   (to:title   )): return .init(id, project, title, text, location, state)
        case let .changing(.text    (to:text    )): return .init(id, project, title, text, location, state)
        case let .changing(.location(to:location)): return .init(id, project, title, text, location, state)
        case let .changing(.state   (to:state   )): return .init(id, project, title, text, location, state)
        }
    }
}
