//
//  SubTask.swift
//  TaskManager
//
//  Created by Manuel Meyer on 15.06.22.
//

import Foundation.NSUUID
import Tools

public func alter(_ s:SubTask, by change:SubTask.Change) -> SubTask { s.alter(by:change) }

public struct SubTask: TypedIdentifiable {
    public typealias IDType = UUID
    public enum Change {
        public enum _Changing {
            case title    (to:String  )
            case text     (to:String  )
            case completed(to:Bool    )
            case task     (to:Task.ID )
            case location (to:Location)
            case state    (to:State   )
        }
        case changing(_Changing)
    }
    public enum State {
        case unfinished
        case finisehd
    }
    public let id       : ID
    public let task     : Task.ID
    public let title    : String
    public let text     : String
    public let completed: Bool
    public let location : Location
    public let state    : State
}
extension SubTask {
    public init(task:Task, title:String) { self.init(Identifier(value:UUID()), task.id, title, "", false, .unknown, .unfinished) }
    public func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
}
extension SubTask: CustomStringConvertible {
    public var description: String { "SubTask «\(id)» on Task «\(task)»: \(title), completed: \(completed)" }
}
extension SubTask      : Codable  {}
extension SubTask.State: Codable  {}
extension SubTask      : Hashable {}

private extension SubTask {
    private init(_ _id:ID,_ _task:Task.ID,_ _title:String,_ _text:String,_ _completed:Bool,_ _location:Location,_ _state:State) { id = _id; task = _task; title = _title; text = _text; completed = _completed; location = _location; state = _state }
    private func alter(_ c:Change) -> Self {
        switch c {
        case let .changing(.task     (to:task     )): return .init(id, task, title, text, completed, location, state)
        case let .changing(.title    (to:title    )): return .init(id, task, title, text, completed, location, state)
        case let .changing(.text     (to:text     )): return .init(id, task, title, text, completed, location, state)
        case let .changing(.completed(to:completed)): return .init(id, task, title, text, completed, location, state)
        case let .changing(.location (to:location )): return .init(id, task, title, text, completed, location, state)
        case let .changing(.state    (to:state    )): return .init(id, task, title, text, completed, location, state)
        }
    }
}
