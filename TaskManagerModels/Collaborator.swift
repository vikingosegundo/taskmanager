//
//  Collaborator.swift
//  TaskManager
//
//  Created by Manuel Meyer on 15.06.22.
//

import Foundation.NSUUID
import Tools

public struct Collaborator: TypedIdentifiable  {
    public typealias IDType = UUID
    public let id  : ID
    public let name: String
    public enum Change {
        case name(String)
    }
    public init(name:String) { self.init(Identifier(value:UUID()), name) }
    public func alter(_ changes: Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
}
extension Collaborator: CustomStringConvertible { public var description:String { "Collaborator «\(id)»: \(name)" } }
extension Collaborator: Codable  {}
extension Collaborator: Hashable {}

private extension Collaborator {
    init(_ _id:ID,_ _name:String) { id = _id; name = _name }
    func alter(by c:Change) -> Self {
        switch c {
        case .name(let name): return .init(id,name)
        }
    }
}
