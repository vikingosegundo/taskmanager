//
//  Project.swift
//  TaskManager
//
//  Created by Manuel Meyer on 15.06.22.
//

import Foundation.NSUUID
import Tools

public func alter(_ p:Project, by change:Project.Change) -> Project { p.alter(by:change) }

public struct Project:TypedIdentifiable {
    public typealias IDType = UUID
    public enum Change {
        public enum _Changing {
            case title   (to:String         )
            case text    (to:String         )
            case owner   (to:Collaborator.ID)
            case location(to:Location       )
        }
        case changing(_Changing)
        case adding  (_Add     )
        case removing(_Remove  )
        public enum _Add    { case collaborator(Collaborator.ID), task(Task.ID) }
        public enum _Remove { case collaborator(Collaborator.ID) }
    }
    public let id           : ID
    public let title        : String
    public let text         : String
    public let owner        : Collaborator.ID
    public let collaborators:[Collaborator.ID]
    public let tasks        :[Task.ID]
    public let location     : Location
    public init(owner:Collaborator,title:String) { self.init(Identifier(value:UUID()), owner.id, title, "", [owner.id],[], .unknown) }
    public func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
}
extension Project: CustomStringConvertible { public var description: String { "Project «\(id)»: \(title)" } }
extension Project: Equatable, Hashable, Codable {}

private extension Project {
    init(_ _id: ID,_ _owner:Collaborator.ID,_ _title:String,_ _text:String,_ _collaborators:[Collaborator.ID],_ _tasks:[Task.ID],_ _location:Location) {
        id = _id; owner = _owner; title = _title; text = _text; collaborators = _collaborators; tasks = _tasks; location = _location
    }
    func alter(by c:Change) -> Self {
        switch c {
        case let .changing(.owner   (to:owner)   ): return .init(id, owner, title, text, collaborators                           ,tasks      , location)
        case let .changing(.title   (to:title)   ): return .init(id, owner, title, text, collaborators                           ,tasks      , location)
        case let .changing(.text    (to:text )   ): return .init(id, owner, title, text, collaborators                           ,tasks      , location)
        case let .adding  (.collaborator(c)      ): return .init(id, owner, title, text, collaborators - [c] + [c]               ,tasks      , location)
        case let .removing(.collaborator(c)      ): return .init(id, owner, title, text,(collaborators - (c == owner ? [] : [c])),tasks      , location)
        case let .adding  (.task        (t)      ): return .init(id, owner, title, text, collaborators                           ,tasks + [t], location)
        case let .changing(.location(to:location)): return .init(id, owner, title, text, collaborators                           ,tasks,       location)
        }
    }
}
