//
//  Message.swift
//  TaskManager
//
//  Created by Manuel Meyer on 23.06.22.
//

import Foundation

public enum Message {
    case taskmanager(_TaskManager)
}

extension Message {
    public enum _TaskManager {
        case add(_Add)
        case remove(_Remove)
        case update(_Update)
        public enum _Add {
            case project(Project)
            case task   (Task   )
            case subtask(SubTask)
            case collaborator(Collaborator)
        }
        public enum _Remove {
            case project(Project)
            case task   (Task   )
            case subtask(SubTask)
        }
        public enum _Update {
            case project(Project)
            case task   (Task   )
            case subtask(SubTask)
        }
    }
}
