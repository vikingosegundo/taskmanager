//
//  DiskStoreSpec.swift
//  TaskManagerTests
//
//  Created by Manuel Meyer on 26.06.22.
//

import Quick
import Nimble
import TaskManagerModels
@testable import TaskManager

fileprivate let state = AppState()
fileprivate let manu  = Collaborator(name:"manu")
fileprivate let otto  = Collaborator(name:"otto")
fileprivate let p0    = Project(owner:manu,title:"Street Festival")
fileprivate let p1    = Project(owner:otto,title:"Book Bazaar")
fileprivate let t00   = Task( project:p0.id,  title:"task 00")

final class DiskStoreSpec: QuickSpec {
    override func spec() {
        describe("DiskStore") {
            var store: Store!
            var currentState: AppState!
            beforeEach {
                store = createDiskStore(pathInDocs: "DiskStoreSpec.json")
                store.updated { currentState = store.state() }
            }
            afterEach {
                currentState = nil
                destroy(&store)
            }
            context("add project and task") {
                beforeEach {
                    store!.change(.by(.adding(.project(p0))))
                    store!.change(.by(.adding(.task(t00))))
                }
                it("project will be present in state") { expect(currentState.projects).to(equal([p0 ])) }
                it("task will be present in state"   ) { expect(currentState.tasks   ).to(equal([t00])) }
                context("reset") {
                    beforeEach {
                        store!.reset()
                    }
                    it("no project will be present") { expect(currentState.projects).to(equal([])) }
                    it("no task will be present")    { expect(currentState.tasks   ).to(equal([])) }
                }
            }
        }
    }
}
