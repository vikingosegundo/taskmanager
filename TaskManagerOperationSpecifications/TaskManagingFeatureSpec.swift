//
//  TaskManagingFeatureSpec.swift
//  TaskManagerTests
//
//  Created by Manuel Meyer on 14.06.22.
//

import Quick
import Nimble
import TaskManagerModels
import TaskManagerUI
import TaskManagerApp

@testable import TaskManager

fileprivate let state = AppState()
fileprivate let manu  = Collaborator(name:"manu")
fileprivate let otto  = Collaborator(name:"otto")
fileprivate let p0    = Project(owner:manu,title:"Street Festival")
fileprivate let p1    = Project(owner:otto,title:"Book Bazaar")
fileprivate let t00   = Task( project:p0.id,  title:"task 00")
fileprivate let t01   = Task( project:p0.id,  title:"task 01")
fileprivate let t10   = Task( project:p1.id,  title:"task 10")
fileprivate let t11   = Task( project:p1.id,  title:"task 11")
fileprivate let s000  = SubTask( task:t00, title:"subtask 0")
fileprivate let s001  = SubTask( task:t00, title:"subtask 1")
fileprivate let s010  = SubTask( task:t01, title:"subtask 0")
fileprivate let s011  = SubTask( task:t01, title:"subtask 1")
fileprivate let s100  = SubTask( task:t10, title:"subtask 0")
fileprivate let s101  = SubTask( task:t10, title:"subtask 1")
fileprivate let s110  = SubTask( task:t11, title:"subtask 0")
fileprivate let s111  = SubTask( task:t11, title:"subtask 1")

final class TaskManagingFeatureSpec: QuickSpec {
    override func spec() {
        var app: Input!
        describe("TaskManager") {
            var store: Store!
            var viewState: ViewState!
            var curentState: AppState?
            beforeEach {
                store = createDiskStore(pathInDocs:"TaskManagingFeature.json")
                viewState = ViewState(store:store)
                store!.updated { curentState = store!.state() }
                app = createAppDomain(store: store, receivers: [], rootHandler: { print(String(describing:$0)) })
            }
            afterEach {
                viewState = nil
                app = nil
                destroy(&store)
            }
            context("adding") {
                context("one project") {
                    beforeEach { app(.taskmanager(.add(.project(p0)))) }
                    it("adds project to state"    ) { expect(curentState?.projects).to          (equal([p0])) }
                    it("adds project to viewstate") { expect(viewState   .projects).toEventually(equal([p0])) }
                    context("another project") {
                        beforeEach { app(.taskmanager(.add(.project(p1)))) }
                        it("adds 2 projects to state"    ) { expect(curentState?.projects).to          (equal([p0,p1])) }
                        it("adds 2 projects to viewstate") { expect(viewState   .projects).toEventually(equal([p0,p1])) }
                    }
                    context("same project again") {
                        beforeEach { app(.taskmanager(.add(.project(p0)))) }
                        it("project will be present just once in state"    ) { expect(curentState?.projects).to          (equal([p0])) }
                        it("project will be present just once in viewstate") { expect(viewState.projects   ).toEventually(equal([p0])) }
                    }
                }
                context("a task") {
                    beforeEach { app(.taskmanager(.add(.task(t00)))) }
                    it("task will be present") { expect(curentState?.tasks).to(equal([t00])) }
                    context("another task") {
                        beforeEach { app(.taskmanager(.add(.task(t01)))) }
                        it("both task will be present") { expect(curentState?.tasks).to(equal([t00, t01])) }
                    }
                    context("same task") {
                        beforeEach { app(.taskmanager(.add(.task(t00)))) }
                        it("task will be present just once") { expect(curentState?.tasks).to(equal([t00])) }
                    }
                }
                context("several tasks for different collaborators") {
                    beforeEach {
                        app(.taskmanager(.add(.task(t00))))
                        app(.taskmanager(.add(.task(t01))))
                        app(.taskmanager(.add(.task(t10))))
                        app(.taskmanager(.add(.task(t11))))
                    }
                    it("tasks will be present") { expect(curentState?.tasks).to(equal([t00,t01,t10,t11])) }
                    context("and add those again") {
                        beforeEach {
                            app(.taskmanager(.add(.task(t00))))
                            app(.taskmanager(.add(.task(t01))))
                            app(.taskmanager(.add(.task(t10))))
                            app(.taskmanager(.add(.task(t11))))
                        }
                        it("tasks will not be added twice") { expect(curentState?.tasks).to(equal([t00,t01,t10,t11])) }
                    }
                }
                context("a subtask") {
                    beforeEach { app(.taskmanager(.add(.subtask(s000)))) }
                    it("subtask will be present") { expect(curentState?.subtasks).to(equal([s000])) }
                    context("another subtask") {
                        beforeEach { app(.taskmanager(.add(.subtask(s001)))) }
                        it("both task will be present") { expect(curentState?.subtasks).to(equal([s000, s001])) }
                    }
                    context("same subtask") {
                        beforeEach { app(.taskmanager(.add(.subtask(s000)))) }
                        it("subtask will be present just once") { expect(curentState?.subtasks).to(equal([s000])) }
                    }
                }
                context("several subtasks") {
                    beforeEach {
                        app(.taskmanager(.add(.subtask(s000))))
                        app(.taskmanager(.add(.subtask(s001))))
                        app(.taskmanager(.add(.subtask(s010))))
                        app(.taskmanager(.add(.subtask(s011))))
                        app(.taskmanager(.add(.subtask(s100))))
                        app(.taskmanager(.add(.subtask(s101))))
                        app(.taskmanager(.add(.subtask(s110))))
                        app(.taskmanager(.add(.subtask(s111))))
                    }
                    it("tasks will be present") { expect(curentState?.subtasks).to(equal([s000,s001,s010,s011,s100,s101,s110,s111])) }
                    context("and add those again") {
                        beforeEach {
                            app(.taskmanager(.add(.subtask(s000))))
                            app(.taskmanager(.add(.subtask(s001))))
                            app(.taskmanager(.add(.subtask(s010))))
                            app(.taskmanager(.add(.subtask(s011))))
                            app(.taskmanager(.add(.subtask(s100))))
                            app(.taskmanager(.add(.subtask(s101))))
                            app(.taskmanager(.add(.subtask(s110))))
                            app(.taskmanager(.add(.subtask(s111))))
                        }
                        it("tasks will not be added twice") { expect(curentState?.subtasks).to(equal([s000,s001,s010,s011,s100,s101,s110,s111])) }
                    }
                }
            }
            context("removing") {
                context("a task") {
                    beforeEach {
                        app(.taskmanager(.add   (.task(t00))))
                        app(.taskmanager(.remove(.task(t00))))
                    }
                    it("task will be not be present") { expect(curentState?.tasks).to(equal([])) }
                    context("another task") {
                        beforeEach {
                            app(.taskmanager(.add   (.task(t00))))
                            app(.taskmanager(.add   (.task(t01))))
                            app(.taskmanager(.remove(.task(t00))))
                            app(.taskmanager(.remove(.task(t01))))
                        }
                        it("tasks will be not be present") { expect(curentState?.tasks).to(equal([])) }
                    }
                }
                context("a unknown task") {
                    beforeEach { app(.taskmanager(.remove(.task(t00)))) }
                    it("task will be not be present") { expect(curentState?.tasks).to(equal([])) }
                }
            }
        }
    }
}
