//
//  AppStateSpec.swift
//  TaskManagerTests
//
//  Created by Manuel Meyer on 26.06.22.
//

import Quick
import Nimble
import TaskManagerModels
@testable import TaskManager

fileprivate let state = AppState()
fileprivate let manu  = Collaborator(name:"manu")
fileprivate let otto  = Collaborator(name:"otto")
fileprivate let p0    = Project(owner:manu,title:"Street Festival")
fileprivate let p1    = Project(owner:otto,title:"Book Bazaar")
fileprivate let t00   = Task( project:p0.id,  title:"task 00")
fileprivate let s000  = SubTask( task:t00, title:"subtask 0")

final class AppStateSpec: QuickSpec {
    override func spec() {
        describe("AppState") {
            context("created") {
                it("with no projects"     ) { expect(state.projects     ).to(beEmpty()) }
                it("with no tasks   "     ) { expect(state.tasks        ).to(beEmpty()) }
                it("with no subtasks"     ) { expect(state.subtasks     ).to(beEmpty()) }
                it("with no collaborators") { expect(state.collaborators).to(beEmpty()) }
            }
            context("Addition") {
                context("Project") {
                    let a = change(state, .by(.adding(.project(p0))))
                    it("project is added"      ) { expect(a.projects     ).to(equal([p0])               ) }
                    it("tasks unchaged"        ) { expect(a.tasks        ).to(equal(state.tasks)        ) }
                    it("subtasks unchaged"     ) { expect(a.subtasks     ).to(equal(state.subtasks)     ) }
                    it("collaborators unchaged") { expect(a.collaborators).to(equal(state.collaborators)) }
                }
                context("Task") {
                    let a = state.alter(.by(.adding(.task(t00))))
                    it("task is added"         ) { expect(a.tasks        ).to(equal([t00])              ) }
                    it("projects unchanged"    ) { expect(a.projects     ).to(equal(state.projects)     ) }
                    it("subtasks unchaged"     ) { expect(a.subtasks     ).to(equal(state.subtasks)     ) }
                    it("collaborators unchaged") { expect(a.collaborators).to(equal(state.collaborators)) }
                }
            }
            context("Removal") {
                context("Project") {
                    let a = change(change(state,.by(.adding(.project(p0)))),.by(.removing(.project(p0))))
                    it("project is removed"    ) { expect(a.projects     ).to(equal([])                 ) }
                    it("tasks unchaged"        ) { expect(a.tasks        ).to(equal(state.tasks)        ) }
                    it("subtasks unchaged"     ) { expect(a.subtasks     ).to(equal(state.subtasks)     ) }
                    it("collaborators unchaged") { expect(a.collaborators).to(equal(state.collaborators)) }
                }
                context("task") {
                    let a = change(change(state,.by(.adding(.task(t00)))),.by(.removing(.task(t00))))
                    it("task is removed"       ) { expect(a.tasks        ).to(equal([])                 ) }
                    it("projects unchanged"    ) { expect(a.projects     ).to(equal(state.projects)     ) }
                    it("subtasks unchaged"     ) { expect(a.subtasks     ).to(equal(state.subtasks)     ) }
                    it("collaborators unchaged") { expect(a.collaborators).to(equal(state.collaborators)) }
                }
            }
            context("Update") {
                context("Project") {
                    let n = p0.alter(by:.changing(.title(to:"updated")))
                    let a = state
                        .alter(.by(.adding  (.project(p0))))
                        .alter(.by(.updating(.project(n ))))
                    it("project is updated"    ) { expect(a.projects     ).to(equal([n])                ) }
                    it("tasks unchaged"        ) { expect(a.tasks        ).to(equal(state.tasks)        ) }
                    it("subtasks unchaged"     ) { expect(a.subtasks     ).to(equal(state.subtasks)     ) }
                    it("collaborators unchaged") { expect(a.collaborators).to(equal(state.collaborators)) }
                }
            }
        }
    }
}
