//
//  createTaskManagerFeature.swift
//  TaskManager
//
//  Created by Manuel Meyer on 23.06.22.
//
import TaskManagerModels

public func createTaskManagerFeature(store: Store, output: @escaping Output) -> Input {
    return { msg in
        func process(cmd: Message._TaskManager) {
            switch cmd {
            case let .add   (.project     (p)): change(store,.by(  .adding(.project(p))))
            case let .add   (   .task     (t)): change(store,.by(  .adding(.task   (t))))
            case let .add   (.subtask     (s)): change(store,.by(  .adding(.subtask(s))))
            case let .add   (.collaborator(c)): change(store,.by(  .adding(.collaborator(c))))

            case let .remove(.project(p)): change(store,.by(.removing(.project(p))))
            case let .remove(   .task(t)): change(store,.by(.removing(.task   (t))))
            case     .remove(.subtask(_)): ()
            case     .update(_)          : ()
            }
        }
        switch msg {
        case .taskmanager(let cmd): process(cmd: cmd)
        }
    }
}
