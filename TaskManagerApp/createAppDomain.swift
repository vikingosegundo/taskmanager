//
//  createAppDomain.swift
//  TaskManager
//
//  Created by Manuel Meyer on 27.06.22.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Foundation
import TaskManagerModels

public typealias  Input = (Message) -> ()
public typealias Output = (Message) -> ()

public func createAppDomain(
    store      : Store,
    receivers  : [Input],
    rootHandler: @escaping Output) -> Input
{
    let features: [Input] = [
        createTaskManagerFeature(store: store,output:rootHandler)
    ]
    return { msg in (receivers + features).forEach { $0(msg) } }
}
